package drahos.dynasty.users.database;

class SqlStatements {

    public static final String selectAllUsers =
                    "select * from users";

    public static final String selectUserByName =
                    "select * from users where username = ?";
}
