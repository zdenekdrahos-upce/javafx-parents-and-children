package drahos.dynasty.users.database;

import drahos.dynasty.connection.OpenedConnection;
import drahos.dynasty.users.entity.User;
import drahos.dynasty.users.entity.UsersGateway;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DbUsersGateway implements UsersGateway {

    private PreparedStatement selectAll, selectById;

    public DbUsersGateway(OpenedConnection connection) {
        try {
            selectAll = connection.prepareStatement(SqlStatements.selectAllUsers);
            selectById = connection.prepareStatement(SqlStatements.selectUserByName);
        } catch (Exception e) {
        }
    }

    @Override
    public User[] findAll() {
        return findByQuery(selectAll);
    }

    @Override
    public User findByName(String name) {
        try {
            selectById.setString(1, name);
        } catch (SQLException e) {
        }
        User[] users = findByQuery(selectById);
        return users.length == 1 ? users[0] : null;
    }

    private User[] findByQuery(PreparedStatement statement) {
        List<User> users = new ArrayList<>();
        try {
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                User u = new User(
                        results.getString("username"),
                        results.getString("password"),
                        "A".equals(results.getString("id_group"))
                        );
                users.add(u);
            }
            results.close();
        } catch (Exception e) {
        }
        return users.toArray(new User[users.size()]);
    }
}
