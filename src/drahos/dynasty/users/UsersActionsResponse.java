package drahos.dynasty.users;

public class UsersActionsResponse {
    public final UserActionsView[] users;

    public UsersActionsResponse(UserActionsView[] users) {
        this.users = users;
    }
}
