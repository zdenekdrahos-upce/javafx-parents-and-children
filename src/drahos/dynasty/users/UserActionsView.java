package drahos.dynasty.users;

import drahos.dynasty.log.ActionView;

public class UserActionsView {

    public final String name;
    public final ActionView[] actions;

    public UserActionsView(String name, ActionView[] actions) {
        this.name = name;
        this.actions = actions;
    }
}
