package drahos.dynasty.users.entity;

public interface UsersGateway {

    User[] findAll();

    User findByName(String name);
}
