package drahos.dynasty.users.entity;

public class User {

    private final String name, password;
    private final boolean isAdmin;

    public User(String name, String password, boolean admin) {
        this.name = name;
        this.password = password;
        isAdmin = admin;
    }

    public String getName() {
        return name;
    }

    public boolean isVisitor(String password) {
        return !isAdmin && isPasswordValid(password);
    }

    public boolean isAdmin(String password) {
        return isAdmin && isPasswordValid(password);
    }

    private boolean isPasswordValid(String password) {
        return this.password.equals(password);
    }
}
