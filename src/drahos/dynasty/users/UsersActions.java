package drahos.dynasty.users;

import drahos.dynasty.log.entity.LogGateway;
import drahos.dynasty.log.entity.LoggedAction;
import drahos.dynasty.users.entity.User;
import drahos.dynasty.users.entity.UsersGateway;
import drahos.dynasty.log.ActionView;

public class UsersActions {
    private final UsersGateway usersGateway;
    private final LogGateway logGateway;

    public UsersActions(UsersGateway usersGateway, LogGateway logGateway) {
        this.usersGateway = usersGateway;
        this.logGateway = logGateway;
    }

    public UsersActionsResponse execute() {
        User[] users = usersGateway.findAll();
        return usersToViews(users);
    }

    private UsersActionsResponse usersToViews(User[] users) {
        UserActionsView[] views = new UserActionsView[users.length];
        for (int i = 0; i < users.length; i++) {
            String username = users[i].getName();
            ActionView[] actions = findUserActions(username);
            views[i] = new UserActionsView(username, actions);
        }
        return new UsersActionsResponse(views);
    }

    private ActionView[] findUserActions(String username) {
        LoggedAction[] actions = logGateway.findUserActions(username);
        return actionsToViews(actions);
    }

    private ActionView[] actionsToViews(LoggedAction[] actions) {
        ActionView[] views = new ActionView[actions.length];
        for (int i = 0; i < actions.length; i++) {
            LoggedAction action = actions[i];
            views[i] = new ActionView(
                    action.getDate(),
                    action.getAction()
            );
        }
        return views;
    }
}
