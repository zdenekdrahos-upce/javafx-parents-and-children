package drahos.dynasty.persons;

import java.util.Date;

public class PersonView {

    public final int id;
    public int age;
    public String firstName, lastName;
    public ParentView father, mother;
    public Date birthDate, deathDate;

    public PersonView(int id) {
        this.id = id;
    }
}

