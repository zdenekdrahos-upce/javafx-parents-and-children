package drahos.dynasty.persons;

import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;

public class Persons {

    private final PersonsGateway gateway;
    private final EntityConverter entityConverter;

    public Persons(PersonsGateway gateway) {
        this.gateway = gateway;
        entityConverter = new EntityConverter(gateway);
    }

    public PersonsResponse execute(PersonsRequest request) {
        Person[] entities;
        if (request.idPerson == null) {
            entities = gateway.findAll();
        } else {
            entities = new Person[] {
                    gateway.findById(request.idPerson)
            };
        }

        PersonView[] persons = entityConverter.personsToView(entities);
        return new PersonsResponse(persons);
    }

}
