package drahos.dynasty.persons.actions.analyze;

public class ChildrenAgeView {

    public final int age, count;

    public ChildrenAgeView(int age, int count) {
        this.age = age;
        this.count = count;
    }

}
