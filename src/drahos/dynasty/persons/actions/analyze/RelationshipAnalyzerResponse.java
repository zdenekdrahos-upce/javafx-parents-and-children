package drahos.dynasty.persons.actions.analyze;

import drahos.dynasty.persons.PersonView;

public class RelationshipAnalyzerResponse {

    public PersonView[] children;
    public PartnerView[] partners;
    public ChildrenAgeView[] childrenAge;
}
