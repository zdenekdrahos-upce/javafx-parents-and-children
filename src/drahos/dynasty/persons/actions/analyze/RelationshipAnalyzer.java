package drahos.dynasty.persons.actions.analyze;

import drahos.dynasty.persons.EntityConverter;
import drahos.dynasty.persons.ParentView;
import drahos.dynasty.persons.PersonView;
import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;

import java.util.HashMap;
import java.util.Map;

public class RelationshipAnalyzer {

    private final PersonsGateway gateway;
    private final EntityConverter entityConverter;
    private int idPerson;
    private Person[] children;
    private RelationshipAnalyzerResponse response;

    public RelationshipAnalyzer(PersonsGateway gateway) {
        this.gateway = gateway;
        entityConverter = new EntityConverter(gateway);
    }

    public RelationshipAnalyzerResponse execute(RelationshipAnalyzerRequest request) {
        findChildren(request);
        response = new RelationshipAnalyzerResponse();
        response.children = convertChildren();
        response.partners = findPartners();
        response.childrenAge = analyzeAge();
        return response;
    }

    private void findChildren(RelationshipAnalyzerRequest request) {
        idPerson = request.idPerson;
        children = gateway.findChildren(idPerson);
    }

    private PersonView[] convertChildren() {
        return entityConverter.personsToView(children);
    }

    private PartnerView[] findPartners() {
        Map<ParentView, Integer> counts = new HashMap<>();
        for (PersonView child : response.children) {
            ParentView partner = child.father.id != null && child.father.id == idPerson ? child.mother : child.father;
            if (!counts.containsKey(partner)) {
                counts.put(partner, 0);
            }
            counts.put(partner, counts.get(partner) + 1);
        }

        int index = 0;
        PartnerView[] partners = new PartnerView[counts.size()];
        for (Map.Entry<ParentView, Integer> parentViewIntegerEntry : counts.entrySet()) {
            partners[index++] = new PartnerView(
                    parentViewIntegerEntry.getKey(),
                    parentViewIntegerEntry.getValue()
            );
        }
        return partners;
    }

    private ChildrenAgeView[] analyzeAge() {
        Map<Integer, Integer> counts = new HashMap<>();
        for (PersonView child : response.children) {
            if (!counts.containsKey(child.age)) {
                counts.put(child.age, 0);
            }
            counts.put(child.age, counts.get(child.age) + 1);
        }
        int index = 0;
        ChildrenAgeView[] ages = new ChildrenAgeView[counts.size()];
        for (Map.Entry<Integer, Integer> entry : counts.entrySet()) {
            ages[index++] = new ChildrenAgeView(
                    entry.getKey(),
                    entry.getValue()
            );
        }
        return ages;
    }
}
