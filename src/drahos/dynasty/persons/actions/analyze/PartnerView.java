package drahos.dynasty.persons.actions.analyze;

import drahos.dynasty.persons.ParentView;

public class PartnerView {

    public final ParentView partner;
    public final int childCount;

    public PartnerView(ParentView parent, int childCount) {
        this.partner = parent;
        this.childCount = childCount;
    }
}
