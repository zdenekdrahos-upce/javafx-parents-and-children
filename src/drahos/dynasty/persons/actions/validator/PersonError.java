package drahos.dynasty.persons.actions.validator;

import drahos.dynasty.error.ApplicationError;

public enum PersonError implements ApplicationError {
    NO_LAST_NAME, NO_BIRTH_DATE, IDENTICAL_PARENTS, DATE_ORDER, NO_CHANGE, NO_FIRST_NAME
}
