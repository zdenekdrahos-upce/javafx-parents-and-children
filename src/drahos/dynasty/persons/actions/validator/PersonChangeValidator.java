package drahos.dynasty.persons.actions.validator;

import drahos.dynasty.persons.entity.Person;

public class PersonChangeValidator {

    public PersonValidationResult validate(Person person, Person updatedPerson) {
        if (isIdentical(person, updatedPerson)) {
            return new PersonValidationResult(new PersonError[] {PersonError.NO_CHANGE});
        } else {
            return new PersonValidationResult(new PersonError[0]);
        }
    }

    private boolean isIdentical(Person person, Person updatedPerson) {
        return person.getFirstName().equals(updatedPerson.getFirstName()) &&
                person.getLastName().equals(updatedPerson.getLastName()) &&
                areComparableObjectsEqual(person.getBirthDate(), updatedPerson.getBirthDate()) &&
                areComparableObjectsEqual(person.getDeathDate(), updatedPerson.getDeathDate()) &&
                areComparableObjectsEqual(person.getIdFather(), updatedPerson.getIdFather()) &&
                areComparableObjectsEqual(person.getIdMother(), updatedPerson.getIdMother());
    }

    private boolean areComparableObjectsEqual(Comparable p1, Comparable p2) {
        boolean isFirstNull = p1 == null;
        boolean isSecondNull = p2 == null;
        return (isFirstNull && isSecondNull) ||
                (!isFirstNull && !isSecondNull && p1.compareTo(p2) == 0);
    }
}
