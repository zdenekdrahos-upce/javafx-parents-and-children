package drahos.dynasty.persons.actions.validator;

public class PersonValidationResult {

    public final PersonError[] errors;

    public PersonValidationResult(PersonError[] errors) {
        this.errors = errors;
    }

    public boolean isValid() {
        return errors.length == 0;
    }
}
