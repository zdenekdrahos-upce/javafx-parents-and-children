package drahos.dynasty.persons.actions.validator;

import drahos.dynasty.persons.entity.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonValidator {

    private Person person;
    private final List<PersonError> errors = new ArrayList<>();

    public PersonValidationResult validate(Person p) {
        beforeValidation(p);
        checkFirstName();
        checkLastName();
        checkBirthDate();
        checkParents();
        checkDateOrder();
        return createResult();
    }

    private void beforeValidation(Person p) {
        person = p;
        errors.clear();
    }

    private void checkFirstName() {
        if (person.getFirstName().isEmpty()) {
            errors.add(PersonError.NO_FIRST_NAME);
        }
    }

    private void checkLastName() {
        if (person.getLastName().isEmpty()) {
            errors.add(PersonError.NO_LAST_NAME);
        }
    }

    private void checkBirthDate() {
        if (person.getBirthDate() == null) {
            errors.add(PersonError.NO_BIRTH_DATE);
        }
    }

    private void checkParents() {
        if (hasIdenticalParents()) {
            errors.add(PersonError.IDENTICAL_PARENTS);
        }
    }

    private boolean hasIdenticalParents() {
        return person.getIdFather() != null && person.getIdMother() != null &&
                person.getIdFather().equals(person.getIdMother());
    }

    private void checkDateOrder() {
        if (hasInvalidDates()) {
            errors.add(PersonError.DATE_ORDER);
        }
    }

    private boolean hasInvalidDates() {
        return person.getBirthDate() != null && person.getDeathDate() != null
                && person.getBirthDate().compareTo(person.getDeathDate()) > 0;
    }

    private PersonValidationResult createResult() {
        PersonError[] errorsArray = errors.toArray(new PersonError[errors.size()]);
        return new PersonValidationResult(errorsArray);
    }
}
