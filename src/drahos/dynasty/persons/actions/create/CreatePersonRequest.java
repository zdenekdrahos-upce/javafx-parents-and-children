package drahos.dynasty.persons.actions.create;

import java.util.Date;

public class CreatePersonRequest {

    public String firstName, lastName;
    public Date birthDate, deathDate;
    public Integer idFather, idMother;
}
