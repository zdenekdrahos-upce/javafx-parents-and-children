package drahos.dynasty.persons.actions.create;

import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.persons.PersonView;

public class CreatePersonResponse {

    public final boolean wasCreated;
    public final PersonView person;
    public final ApplicationError[] errors;

    public CreatePersonResponse(PersonView person) {
        this.wasCreated = true;
        this.person = person;
        this.errors = new ApplicationError[0];
    }

    public CreatePersonResponse(ApplicationError[] errors) {
        this.wasCreated = false;
        this.person = null;
        this.errors = errors;
    }
}
