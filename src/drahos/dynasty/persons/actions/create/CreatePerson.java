package drahos.dynasty.persons.actions.create;

import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.error.DbError;
import drahos.dynasty.persons.Persons;
import drahos.dynasty.persons.PersonsRequest;
import drahos.dynasty.persons.PersonsResponse;
import drahos.dynasty.persons.actions.validator.PersonValidationResult;
import drahos.dynasty.persons.actions.validator.PersonValidator;
import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;
import drahos.dynasty.persons.PersonView;

public class CreatePerson {

    private final PersonsGateway gateway;
    private final Persons persons;
    private final PersonsRequest personsRequest;
    private final PersonValidator validator;

    public CreatePerson(PersonsGateway gateway) {
        this.gateway = gateway;
        persons = new Persons(gateway);
        personsRequest = new PersonsRequest();
        validator = new PersonValidator();
    }

    public CreatePersonResponse execute(CreatePersonRequest request) {
        Person person = requestToPerson(request);
        PersonValidationResult validationResult = validator.validate(person);
        if (validationResult.isValid()) {
            if (gateway.create(person)) {
                PersonView view = findPersonById(person);
                return new CreatePersonResponse(view);
            } else {
                return new CreatePersonResponse(new ApplicationError[] {DbError.DATABASE_ERROR});
            }
        } else {
            return new CreatePersonResponse(validationResult.errors);
        }
    }

    private Person requestToPerson(CreatePersonRequest request) {
        return new Person(
                0,
                request.firstName,
                request.lastName,
                request.birthDate,
                request.deathDate,
                request.idFather,
                request.idMother
        );
    }

    private PersonView findPersonById(Person person) {
        personsRequest.idPerson = person.getId();
        PersonsResponse response = persons.execute(personsRequest);
        if (response.persons.length == 1) {
            return response.persons[0];
        } else {
            return null;
        }
    }
}
