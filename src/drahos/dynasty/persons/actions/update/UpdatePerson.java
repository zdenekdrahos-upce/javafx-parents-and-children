package drahos.dynasty.persons.actions.update;

import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.error.DbError;
import drahos.dynasty.persons.PersonView;
import drahos.dynasty.persons.actions.validator.PersonChangeValidator;
import drahos.dynasty.persons.actions.validator.PersonValidationResult;
import drahos.dynasty.persons.actions.validator.PersonValidator;
import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UpdatePerson {

    private final PersonsGateway gateway;
    private final PersonValidator personValidator;
    private final PersonChangeValidator changeValidator;
    private final List<ApplicationError> errors = new ArrayList<>();

    public UpdatePerson(PersonsGateway gateway) {
        this.gateway = gateway;
        personValidator = new PersonValidator();
        changeValidator = new PersonChangeValidator();
    }

    public UpdatePersonResponse execute(UpdatePersonRequest request) {
        Person person = gateway.findById(request.updatedPerson.id);
        Person newPerson = viewToPerson(request.updatedPerson);

        validate(person, newPerson);

        if (errors.isEmpty()) {
            if (!gateway.update(newPerson)) {
                errors.add(DbError.DATABASE_ERROR);
            }
        }
        return createResponse();
    }

    private void validate(Person person, Person newPerson) {
        errors.clear();
        validatePerson(newPerson);
        validateChange(person, newPerson);
    }

    private void validatePerson(Person newPerson) {
        PersonValidationResult result = personValidator.validate(newPerson);
        checkValidationResult(result);
    }

    private void validateChange(Person person, Person updatedPerson) {
        PersonValidationResult result = changeValidator.validate(person, updatedPerson);
        checkValidationResult(result);
    }

    private void checkValidationResult(PersonValidationResult result) {
        if (!result.isValid()) {
            errors.addAll(Arrays.asList(result.errors));
        }
    }

    private Person viewToPerson(PersonView updatedPerson) {
        return new Person(
                updatedPerson.id,
                updatedPerson.firstName,
                updatedPerson.lastName,
                updatedPerson.birthDate,
                updatedPerson.deathDate,
                updatedPerson.father.id,
                updatedPerson.mother.id
        );
    }

    private UpdatePersonResponse createResponse() {
        ApplicationError[] errorsArray = errors.toArray(new ApplicationError[errors.size()]);
        return new UpdatePersonResponse(errorsArray);
    }
}
