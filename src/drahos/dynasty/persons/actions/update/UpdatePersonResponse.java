package drahos.dynasty.persons.actions.update;

import drahos.dynasty.error.ApplicationError;

public class UpdatePersonResponse {

    public final boolean wasUpdated;
    public final ApplicationError[] errors;

    public UpdatePersonResponse(ApplicationError[] errors) {
        this.wasUpdated = errors.length == 0;
        this.errors = errors;
    }
}
