package drahos.dynasty.persons.actions.delete;

public class DeletePersonResponse {
    public final boolean wasDeleted;

    public DeletePersonResponse(boolean wasDeleted) {
        this.wasDeleted = wasDeleted;
    }
}
