package drahos.dynasty.persons.actions.delete;

import drahos.dynasty.persons.entity.PersonsGateway;

public class DeletePerson {

    private final PersonsGateway gateway;

    public DeletePerson(PersonsGateway gateway) {
        this.gateway = gateway;
    }

    public DeletePersonResponse execute(DeletePersonRequest request) {
        boolean wasDeleted = gateway.deleteById(request.idPerson);
        return new DeletePersonResponse(wasDeleted);
    }
}
