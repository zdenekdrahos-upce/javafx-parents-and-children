package drahos.dynasty.persons.database;

import drahos.dynasty.connection.OpenedConnection;
import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DbPersonsGateway implements PersonsGateway {

    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private PreparedStatement selectPersons, selectChildren, selectById, deleteById, insertPerson, updatePerson;

    public DbPersonsGateway(OpenedConnection connection) {
        try {
            selectPersons = connection.prepareStatement(SqlStatements.selectAllPersons);
            selectChildren = connection.prepareStatement(SqlStatements.selectChildren);
            selectById = connection.prepareStatement(SqlStatements.selectPerson);
            deleteById = connection.prepareStatement(SqlStatements.deletePerson);
            insertPerson = connection.prepareStatement(SqlStatements.insertPerson, new String[] { "id_person" });
            updatePerson = connection.prepareStatement(SqlStatements.updatePerson);
        } catch (Exception e) {
        }
    }

    @Override
    public Person[] findAll() {
        return findByQuery(selectPersons);
    }

    @Override
    public Person[]  findChildren(int idParent) {
        try {
            selectChildren.setInt(1, idParent);
            selectChildren.setInt(2, idParent);
        } catch (SQLException e) {
        }
        return findByQuery(selectChildren);
    }

    @Override
    public boolean create(Person person) {
        try {
            loadPersonStatement(person, insertPerson);

            int updatedRowsCount = insertPerson.executeUpdate();
            if (updatedRowsCount == 1) {
                ResultSet generatedKeys = insertPerson.getGeneratedKeys();
                generatedKeys.next();
                person.setId(generatedKeys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
        }
        return false;
    }

    @Override
    public boolean deleteById(int idPerson) {
        try {
            deleteById.setInt(1, idPerson);
            int deletedRowsCount = deleteById.executeUpdate();
            return deletedRowsCount == 1;
        } catch (SQLException e) {
        }
        return false;
    }

    @Override
    public boolean update(Person person) {
        try {
            updatePerson.setInt(7, person.getId());
            loadPersonStatement(person, updatePerson);

            int updatedRowsCount = updatePerson.executeUpdate();
            return updatedRowsCount == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void loadPersonStatement(Person person, PreparedStatement statement) throws SQLException {
        statement.setString(1, person.getFirstName());
        statement.setString(2, person.getLastName());
        statement.setDate(3, new java.sql.Date(person.getBirthDate().getTime()));
        loadNullableDate(statement, person.getDeathDate());
        loadParentId(statement, person.getIdFather(), 5);
        loadParentId(statement, person.getIdMother(), 6);
    }

    private void loadNullableDate(PreparedStatement statement, Date death) throws SQLException {
        if (death == null) {
            statement.setNull(4, Types.DATE);
        } else {
            statement.setDate(4, new java.sql.Date(death.getTime()));
        }
    }

    private void loadParentId(PreparedStatement statement, Integer idMother, int columnsIndex) throws SQLException {
        if (idMother == null) {
            statement.setNull(columnsIndex, Types.NUMERIC);
        } else {
            statement.setInt(columnsIndex, idMother);
        }
    }

    @Override
    public Person findById(int idPerson) {
        try {
            selectById.setInt(1, idPerson);
        } catch (SQLException e) {
        }
        Person[] persons = findByQuery(selectById);
        return persons.length == 1 ? persons[0] : null;
    }

    private Person[] findByQuery(PreparedStatement statement) {
        List<Person> persons = new ArrayList<>();
        try {
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                Person p = new Person(
                        results.getInt("id_person"),
                        results.getString("first_name"),
                        results.getString("last_name"),
                        getDate(results.getString("birth_date")),
                        getDate(results.getString("death_date")),
                        getInteger(results, "id_father"),
                        getInteger(results, "id_mother")
                );
                persons.add(p);
            }
            results.close();
        } catch (Exception e) {
        }
        return persons.toArray(new Person[persons.size()]);
    }

    private Integer getInteger(ResultSet results, String column) throws SQLException {
        return results.getObject(column) == null ? null : results.getInt(column);
    }

    private Date getDate(String dbDate) {
        try {
            if (dbDate != null) {
                return dateFormatter.parse(dbDate);
            }
        } catch (ParseException e) {
        }
        return null;
    }
}
