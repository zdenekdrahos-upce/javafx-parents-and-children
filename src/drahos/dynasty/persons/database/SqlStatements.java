package drahos.dynasty.persons.database;

class SqlStatements {

    public final static String selectAllPersons =
            "select p.*, father.id_person as id_father, mother.id_person as id_mother " +
                    "from persons p " +
                    "left join persons father on p.id_father = father.id_person " +
                    "left join persons mother on p.id_mother = mother.id_person " +
                    "order by p.birth_date asc, id_person asc";

    public final static String selectChildren =
            "select p.*, father.id_person as id_father, mother.id_person as id_mother " +
                    "from persons p " +
                    "left join persons father on p.id_father = father.id_person " +
                    "left join persons mother on p.id_mother = mother.id_person " +
                    "where  father.id_person = ? or mother.id_person = ?" +
                    "order by p.birth_date asc, id_person asc";

    public static final String selectPerson =
            "select p.*, father.id_person as id_father, mother.id_person as id_mother " +
                    "from persons p " +
                    "left join persons father on p.id_father = father.id_person " +
                    "left join persons mother on p.id_mother = mother.id_person " +
                    "where p.id_person = ?" +
                    "order by p.birth_date asc, id_person asc";

    public static final String insertPerson =
            "insert into persons(id_person, first_name, last_name, birth_date, death_date, id_father, id_mother) " +
                    "values (SEQ_PERSONS_id_person.nextval, ?, ?, ?, ?, ?, ?) ";

    public static final String updatePerson =
            "update persons set " +
                    "first_name = ?, " +
                    "last_name = ?, " +
                    "birth_date = ?, " +
                    "death_date = ?, " +
                    "id_father = ?, " +
                    "id_mother = ? " +
                    "where id_person = ?";

    public static final String deletePerson =
            "delete from persons where id_person = ?";
}
