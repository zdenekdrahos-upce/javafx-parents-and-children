package drahos.dynasty.persons;

import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;

import java.util.HashMap;
import java.util.Map;

public class EntityConverter {

    private final PersonsGateway gateway;
    private final Map<Integer, ParentView> parents = new HashMap<>();

    public EntityConverter(PersonsGateway gateway) {
        this.gateway = gateway;
    }

    public PersonView[] personsToView(Person[] entities) {
        parents.clear();
        PersonView[] persons = new PersonView[entities.length];
        int id = 0;
        for (Person p : entities) {
            persons[id++] = personToView(p);
        }
        return persons;
    }

    private PersonView personToView(Person p) {
        PersonView view = new PersonView(p.getId());
        view.firstName = p.getFirstName();
        view.lastName = p.getLastName();
        view.age = p.getAge();
        view.birthDate = p.getBirthDate();
        view.deathDate = p.getDeathDate();
        view.father = getParent(p.getIdFather());
        view.mother = getParent(p.getIdMother());
        return view;
    }

    private ParentView getParent(Integer idParent) {
        if (idParent != null) {
            if (parents.containsKey(idParent)) {
                return parents.get(idParent);
            } else {
                Person parent = gateway.findById(idParent);
                if (parent != null) {
                    ParentView p = new ParentView(parent);
                    parents.put(idParent, p);
                    return p;
                }
            }
        }
        return ParentView.noParent;
    }
}
