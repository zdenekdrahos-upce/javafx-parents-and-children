package drahos.dynasty.persons;

import drahos.dynasty.persons.entity.Person;

public class ParentView {

    public final Integer id;
    public final String name;

    public final static ParentView noParent = new ParentView();

    private ParentView() {
        id = null;
        name = "";
    }

    public ParentView(Person parent) {
        id = parent.getId();
        name = parent.getFirstName() + " " + parent.getLastName();
    }

    public ParentView(PersonView parent) {
        id = parent.id;
        name = parent.firstName + " " + parent.lastName;
    }
}
