package drahos.dynasty.persons;

public class PersonsResponse {

    public final PersonView[] persons;

    public PersonsResponse(PersonView[] persons) {
        this.persons = persons;
    }
}
