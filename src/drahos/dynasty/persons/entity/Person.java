package drahos.dynasty.persons.entity;

import java.util.Calendar;
import java.util.Date;

public class Person {

    private int id;
    private Integer idFather, idMother;
    private String firstName, lastName;
    private Date birth, death;

    public Person(int id, String firstName, String lastName, Date birth, Date death, Integer idFather, Integer idMother) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birth = birth;
        this.death = death;
        this.idFather = idFather;
        this.idMother = idMother;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birth;
    }

    public Date getDeathDate() {
        return death;
    }

    public Integer getIdFather() {
        return idFather;
    }

    public Integer getIdMother() {
        return idMother;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdFather(Integer idFather) {
        this.idFather = idFather;
    }

    public void setIdMother(Integer idMother) {
        this.idMother = idMother;
    }

    public int getAge() {
        Calendar birthCalendar = Calendar.getInstance();
        Calendar currentCalendar = Calendar.getInstance();
        birthCalendar.setTime(birth);
        if (death != null) {
            currentCalendar.setTime(death);
        }
        int factor = 0;
        if(currentCalendar.get(Calendar.DAY_OF_YEAR) < birthCalendar.get(Calendar.DAY_OF_YEAR)) {
            factor = -1;
        }
        return currentCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR) + factor;
    }
}
