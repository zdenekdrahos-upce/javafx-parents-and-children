package drahos.dynasty.persons.entity;

public interface PersonsGateway {
    Person[] findAll();

    Person[] findChildren(int idParent);

    Person findById(int idPerson);

    boolean create(Person person);

    boolean update(Person person);

    boolean deleteById(int idPerson);
}
