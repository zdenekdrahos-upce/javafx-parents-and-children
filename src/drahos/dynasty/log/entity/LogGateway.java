package drahos.dynasty.log.entity;

public interface LogGateway {

    void insertAction(String username, String action);

    LoggedAction[] findUserActions(String username);
}
