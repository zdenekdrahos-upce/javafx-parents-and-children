package drahos.dynasty.log.entity;

import java.util.Date;

public class LoggedAction {

    private final String username, action;
    private final Date date;

    public LoggedAction(String username, String action, Date date) {
        this.username = username;
        this.action = action;
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public String getAction() {
        return action;
    }

    public Date getDate() {
        return date;
    }
}
