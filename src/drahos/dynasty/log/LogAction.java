package drahos.dynasty.log;

import drahos.dynasty.log.entity.LogGateway;

public class LogAction {

    private final LogGateway gateway;

    public LogAction(LogGateway gateway) {
        this.gateway = gateway;
    }

    public void execute(LogActionRequest request) {
        gateway.insertAction(request.username, request.action);
    }
}
