package drahos.dynasty.log.database;

class SqlStatements {

    public static final String insertLogAction =
                    "insert into log(username, time, action) values (?, sysdate, ?)";

    public static final String selectUserLogActions =
                    "select * from log where username = ? order by time desc";
}
