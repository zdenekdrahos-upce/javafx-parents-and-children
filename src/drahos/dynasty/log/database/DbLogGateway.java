package drahos.dynasty.log.database;

import drahos.dynasty.connection.OpenedConnection;
import drahos.dynasty.log.entity.LogGateway;
import drahos.dynasty.log.entity.LoggedAction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DbLogGateway implements LogGateway {

    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private PreparedStatement insertAction, selectUserActions;

    public DbLogGateway(OpenedConnection connection) {
        try {
            insertAction = connection.prepareStatement(SqlStatements.insertLogAction);
            selectUserActions = connection.prepareStatement(SqlStatements.selectUserLogActions);
        } catch (Exception e) {
        }
    }

    @Override
    public void insertAction(String username, String action) {
        try {
            insertAction.setString(1, username);
            insertAction.setString(2, action);
            insertAction.executeUpdate();
        } catch (SQLException e) {
        }
    }

    @Override
    public LoggedAction[] findUserActions(String username) {
        try {
            selectUserActions.setString(1, username);
        } catch (SQLException e) {
        }
        return findByQuery(selectUserActions);
    }

    private LoggedAction[] findByQuery(PreparedStatement statement) {
        List<LoggedAction> actions = new ArrayList<>();
        try {
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                LoggedAction a = new LoggedAction(
                        results.getString("username"),
                        results.getString("action"),
                        getDate(results.getString("time"))
                );
                actions.add(a);
            }
            results.close();
        } catch (Exception e) {
        }
        return actions.toArray(new LoggedAction[actions.size()]);
    }

    private Date getDate(String dbDate) {
        try {
            if (dbDate != null) {
                return dateFormatter.parse(dbDate);
            }
        } catch (ParseException e) {
        }
        return null;
    }
}
