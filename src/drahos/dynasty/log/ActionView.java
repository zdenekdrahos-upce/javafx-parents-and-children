package drahos.dynasty.log;

import java.util.Date;

public class ActionView {

    public final Date date;
    public final String action;

    public ActionView(Date date, String action) {
        this.date = date;
        this.action = action;
    }
}
