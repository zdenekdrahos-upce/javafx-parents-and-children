package drahos.dynasty;

import drahos.dynasty.auth.encoder.PlainPassword;
import drahos.dynasty.connection.JdbcConnection;
import drahos.dynasty.connection.OpenedConnection;
import drahos.dynasty.persons.actions.analyze.RelationshipAnalyzer;
import drahos.dynasty.persons.actions.update.UpdatePerson;
import drahos.dynasty.persons.database.DbPersonsGateway;
import drahos.dynasty.users.database.DbUsersGateway;
import drahos.dynasty.log.database.DbLogGateway;
import drahos.dynasty.log.entity.LogGateway;
import drahos.dynasty.persons.entity.PersonsGateway;
import drahos.dynasty.users.entity.UsersGateway;
import drahos.dynasty.persons.actions.create.CreatePerson;
import drahos.dynasty.persons.actions.delete.DeletePerson;
import drahos.dynasty.auth.Authentication;
import drahos.dynasty.auth.entity.PasswordEncoder;
import drahos.dynasty.persons.Persons;
import drahos.dynasty.log.LogAction;
import drahos.dynasty.users.UsersActions;

public class InteractorFactory {

    private OpenedConnection connection;

    public InteractorFactory() {
        connection = JdbcConnection.getConnection();
    }

    public Persons buildPersons() {
        PersonsGateway personsGateway = new DbPersonsGateway(connection);
        return new Persons(personsGateway);
    }

    public Authentication buildAuthentication() {
        PasswordEncoder encoder = new PlainPassword();
        UsersGateway usersGateway = new DbUsersGateway(connection);
        return new Authentication(usersGateway, encoder);
    }

    public UsersActions buildUsersActions() {
        LogGateway logGateway = new DbLogGateway(connection);
        UsersGateway usersGateway = new DbUsersGateway(connection);
        return new UsersActions(usersGateway, logGateway);
    }

    public LogAction buildLoginAction() {
        LogGateway logGateway = new DbLogGateway(connection);
        return new LogAction(logGateway);
    }

    public CreatePerson buildPersonCreate() {
        PersonsGateway personsGateway = new DbPersonsGateway(connection);
        return new CreatePerson(personsGateway);
    }

    public DeletePerson buildPersonDelete() {
        PersonsGateway personsGateway = new DbPersonsGateway(connection);
        return new DeletePerson(personsGateway);
    }

    public UpdatePerson buildPersonUpdate() {
        PersonsGateway personsGateway = new DbPersonsGateway(connection);
        return new UpdatePerson(personsGateway);
    }

    public RelationshipAnalyzer buildRelationshipAnalyzer() {
        PersonsGateway personsGateway = new DbPersonsGateway(connection);
        return new RelationshipAnalyzer(personsGateway);
    }
}
