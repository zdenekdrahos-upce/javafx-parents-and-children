package drahos.dynasty.connection;

import java.sql.PreparedStatement;

public interface OpenedConnection {

    PreparedStatement prepareStatement(String sql);

    PreparedStatement prepareStatement(String sql, String[] keys);
}
