package drahos.dynasty.connection;

import oracle.jdbc.OracleDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcConnection implements OpenedConnection {

    private static JdbcConnection instance;

    public static void openConnection(String connectionString, String username, String password) {
        if (instance == null) {
            instance = new JdbcConnection(connectionString, username, password);
        }
    }

    public static OpenedConnection getConnection() {
        return instance;
    }

    public static void closeConnection() {
        instance.close();
    }

    private final Connection connection;
    private final List<PreparedStatement> preparedStatements;

    private JdbcConnection(String connectionString, String username, String password) {
        preparedStatements = new ArrayList<>();
        try {
            DriverManager.registerDriver(new OracleDriver());
            connection = DriverManager.getConnection(connectionString, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PreparedStatement prepareStatement(String sql) {
        PreparedStatement s = null;
        try {
            s = connection.prepareStatement(sql);
            preparedStatements.add(s);
        } catch (SQLException e) {
        }
        return s;
    }

    @Override
    public PreparedStatement prepareStatement(String sql, String[] keys) {
        PreparedStatement s = null;
        try {
            s = connection.prepareStatement(sql, keys);
            preparedStatements.add(s);
        } catch (SQLException e) {
        }
        return s;
    }

    private void close() {
        try {
            closeStatements();
            connection.close();
        } catch (SQLException e) {
        }
    }

    private void closeStatements() throws SQLException {
        for (PreparedStatement statement : preparedStatements) {
            statement.close();
        }
        preparedStatements.clear();
    }
}
