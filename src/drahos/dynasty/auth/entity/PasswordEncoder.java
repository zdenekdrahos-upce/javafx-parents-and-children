package drahos.dynasty.auth.entity;

public interface PasswordEncoder {

    String encode(String password);

}
