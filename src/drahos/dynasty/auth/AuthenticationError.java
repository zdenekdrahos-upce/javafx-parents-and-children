package drahos.dynasty.auth;

import drahos.dynasty.error.ApplicationError;

public enum AuthenticationError implements ApplicationError {
    NONEXISTING_USERNAME, UNATHORIZED_ACCESS
}
