package drahos.dynasty.auth;

public class AuthenticationResponse {

    public final boolean isAuthenticated;
    public final AuthenticationError error;

    public AuthenticationResponse(AuthenticationError error) {
        isAuthenticated = error == null;
        this.error = error;
    }
}
