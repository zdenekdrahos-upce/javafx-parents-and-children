package drahos.dynasty.auth;

import drahos.dynasty.auth.entity.PasswordEncoder;
import drahos.dynasty.users.entity.User;
import drahos.dynasty.users.entity.UsersGateway;

public class Authentication {

    private final UsersGateway gateway;
    private final PasswordEncoder passwordEncoder;

    public Authentication(UsersGateway gateway, PasswordEncoder passwordEncoder) {
        this.gateway = gateway;
        this.passwordEncoder = passwordEncoder;
    }

    public AuthenticationResponse execute(AuthenticationRequest request) {
        AuthenticationError authenticationError = null;
        User user = gateway.findByName(request.username);
        if (user == null) {
            authenticationError = AuthenticationError.NONEXISTING_USERNAME;
        } else if (isUnauthorized(request, user)) {
            authenticationError = AuthenticationError.UNATHORIZED_ACCESS;
        }
        return new AuthenticationResponse(authenticationError);
    }

    private boolean isUnauthorized(AuthenticationRequest request, User user) {
        String password = passwordEncoder.encode(request.password);
        boolean isAuthenticated = request.authenticateAsAdmin ? user.isAdmin(password) : user.isVisitor(password);
        return !isAuthenticated;
    }
}
