package drahos.dynasty.auth.encoder;

import drahos.dynasty.auth.entity.PasswordEncoder;

public class PlainPassword implements PasswordEncoder {

    @Override
    public String encode(String password) {
        return password;
    }

}
