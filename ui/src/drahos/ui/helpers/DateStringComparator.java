package drahos.ui.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class DateStringComparator implements Comparator<String> {

    private final SimpleDateFormat format;

    public DateStringComparator(String format) {
        this.format = new SimpleDateFormat(format);
    }

    @Override
    public int compare(String a, String b) {
        boolean isAEmpty = a.equals("");
        boolean isBEmpty = b.equals("");
        if (isAEmpty && isBEmpty) {
            return 0;
        } else if (isAEmpty) {
            return 1;
        } else if (isBEmpty) {
            return -1;
        } else {
            return compareDates(a, b);
        }
    }

    private int compareDates(String a, String b) {
        try {
            Date dateA = format.parse(a);
            Date dateB = format.parse(b);
            return Long.compare(dateA.getTime(), dateB.getTime());
        } catch (ParseException e) {
            return 0;
        }
    }
}
