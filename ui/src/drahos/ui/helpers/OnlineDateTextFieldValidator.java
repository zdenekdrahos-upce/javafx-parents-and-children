package drahos.ui.helpers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class OnlineDateTextFieldValidator implements ChangeListener<String> {

    private final SimpleDateFormat dateFormat;
    private final TextField field;

    public OnlineDateTextFieldValidator(TextField field, SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
        this.field = field;
        field.setPromptText(dateFormat.toPattern());
    }

    @Override
    public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
        if (isValidDate(newValue)) {
            field.getStyleClass().removeAll("error");
        } else {
            field.getStyleClass().add("error");
        }
    }

    private boolean isValidDate(String date) {
        if (!date.isEmpty()) {
            try {
                dateFormat.parse(date);
            } catch (ParseException e) {
                return false;
            }
        }
        return true;
    }
}
