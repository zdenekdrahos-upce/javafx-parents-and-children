package drahos.ui.helpers;

import drahos.ui.components.personstable.PersonRow;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FilterableParents {

    private final List<PersonRow> allParents = new ArrayList<>();
    private final ObservableList<PersonRow> availableParents = FXCollections.observableArrayList();

    public FilterableParents(ObservableList<PersonRow> personsTable) {
        addPersons(personsTable);
        personsTable.addListener(new PersonsListener());
    }

    public ObservableList<PersonRow> getAvailableParents() {
        return availableParents;
    }

    private void addPersons(List personsTable) {
        for (Object row : personsTable) {
            PersonRow person = (PersonRow) row;
            availableParents.add(person);
            allParents.add(person);
        }
    }

    private void deletePersons(List personsTable) {
        for (Object row : personsTable) {
            availableParents.remove(row);
        }
    }

    public void filterByPerson(PersonRow person) {
        removeFilter();
        for (Iterator<PersonRow> iterator = availableParents.iterator(); iterator.hasNext(); ) {
            PersonRow parent = iterator.next();
            if (person.getIdPerson() == parent.getIdPerson() || parent.isYounger(person)) {
                iterator.remove();
            }
        }
    }

    public void filterByDate(Date date) {
        removeFilter();
        for (Iterator<PersonRow> iterator = availableParents.iterator(); iterator.hasNext(); ) {
            PersonRow parent = iterator.next();
            if (parent.isYounger(date)) {
                iterator.remove();
            }
        }
    }

    public void removeFilter() {
        availableParents.clear();
        availableParents.addAll(allParents);
    }

    private class PersonsListener implements ListChangeListener<PersonRow> {

        @Override
        public void onChanged(Change<? extends PersonRow> change) {
            while (change.next()) {
                if (change.wasAdded()) {
                    List<? extends PersonRow> added = change.getAddedSubList();
                    addPersons(added);
                }
                if (change.wasRemoved()) {
                    List<? extends PersonRow> removed = change.getRemoved();
                    deletePersons(removed);
                }
            }
        }
    }
}
