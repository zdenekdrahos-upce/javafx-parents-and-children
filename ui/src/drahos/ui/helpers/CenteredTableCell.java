package drahos.ui.helpers;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

public class CenteredTableCell implements Callback<TableColumn, TableCell> {

    @Override
    public TableCell call(TableColumn tableColumn) {
        TableCell c = new TextCell();
        c.setAlignment(Pos.BASELINE_CENTER);
        return c;
    }

    private static class TextCell extends TableCell {
        @Override
        protected void updateItem(Object o, boolean b) {
            if (o != null) {
                setText(o.toString());
            }
        }
    }

}
