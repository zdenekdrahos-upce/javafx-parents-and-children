package drahos.ui.components.personstable;

import drahos.dynasty.persons.PersonView;
import drahos.ui.helpers.DateStringComparator;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;

public class TableController {

    @FXML
    private TableView table;
    @FXML
    private TableColumn birthColumn, deathColumn;

    private final ObservableList<PersonRow> rows = FXCollections.observableArrayList();
 
    public void initialize() {
        setRows();
        setTablePlaceholder();
        addComparatorToDateColumns();
    }

    private void setRows() {
        table.setItems(rows);
    }

    private void setTablePlaceholder() {
        table.setPlaceholder(new Text("Nikdo :("));
    }

    private void addComparatorToDateColumns() {
        DateStringComparator comparator = new DateStringComparator("dd.MM.yyyy");
        birthColumn.setComparator(comparator);
        deathColumn.setComparator(comparator);
    }

    public void insertPersons(PersonView[] persons) {
        for (PersonView person : persons) {
            PersonRow r = new PersonRow(person);
            rows.add(r);
        }
    }

    public void clearRows() {
        rows.clear();
    }

    public ObservableList<PersonRow> getRows() {
        return rows;
    }

    public void listenOnSelectedRow(ChangeListener changeListener) {
        TableView.TableViewSelectionModel selectionModel = table.getSelectionModel();
        ReadOnlyObjectProperty selectedRow = selectionModel.selectedItemProperty();
        selectedRow.addListener(changeListener);
    }

    public ObservableList getColumns() {
        return table.getColumns();
    }

    public void enableEditation() {
        table.setEditable(true);
    }
}
