package drahos.ui.components.personstable;

import drahos.dynasty.persons.ParentView;
import drahos.dynasty.persons.PersonView;
import javafx.beans.property.SimpleStringProperty;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PersonRow {

    private static final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    private final PersonView person;
    private final SimpleStringProperty firstName, lastName, father, mother, birthDate, deathDate;

    public PersonRow(PersonView person) {
        this.person = person;
        firstName = new SimpleStringProperty(person.firstName);
        lastName = new SimpleStringProperty(person.lastName);
        birthDate = new SimpleStringProperty(getDate(person.birthDate));
        deathDate = new SimpleStringProperty(getDate(person.deathDate));
        father = new SimpleStringProperty(person.father.name);
        mother = new SimpleStringProperty(person.mother.name);
    }

    private String getDate(Date date) {
        if (date != null) {
            return dateFormat.format(date);
        }
        return "";
    }

    private Date getDate(String date) {
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
        }
        return null;
    }

    public int getIdPerson() {
        return person.id;
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public SimpleStringProperty fatherProperty() {
        return father;
    }

    public SimpleStringProperty motherProperty() {
        return mother;
    }

    public SimpleStringProperty birthDateProperty() {
        return birthDate;
    }

    public SimpleStringProperty deathDateProperty() {
        return deathDate;
    }

    public boolean hasParent(PersonRow parent) {
        return isFather(parent) || isMother(parent);
    }

    private boolean isFather(PersonRow parent) {
        return person.father.id != null && parent.getIdPerson() == person.father.id;
    }

    private boolean isMother(PersonRow parent) {
        return person.mother.id != null && parent.getIdPerson() == person.mother.id;
    }

    public void deleteParent(PersonRow parent) {
        if (isFather(parent)) {
            person.father = ParentView.noParent;
            father.setValue("");
        } else {
            person.mother = ParentView.noParent;
            mother.setValue("");
        }
    }

    public void updateParent(PersonRow parent) {
        if (isFather(parent)) {
            father.setValue(parent.toString());
        } else {
            mother.setValue(parent.toString());
        }
    }

    public void changeFather(PersonRow newParent) {
        person.father = new ParentView(newParent.person);
        father.setValue(newParent.toString());
    }

    public void changeMother(PersonRow newParent) {
        person.mother = new ParentView(newParent.person);
        mother.setValue(newParent.toString());
    }

    public boolean isYounger(PersonRow date) {
        return isYounger(date.person.birthDate);
    }

    public boolean isYounger(Date date) {
        return person.birthDate.compareTo(date) > 0;
    }

    @Override
    public String toString() {
        return person.firstName + " " + person.lastName + " (" + getDate(person.birthDate) + ")";
    }

    public PersonView getUpdatePerson() {
        person.firstName = firstName.getValue();
        person.lastName = lastName.getValue();
        person.birthDate = getDate(birthDate.getValue());
        person.deathDate = getDate(deathDate.getValue());
        // parents are set in other methods
        return person;
    }
}
