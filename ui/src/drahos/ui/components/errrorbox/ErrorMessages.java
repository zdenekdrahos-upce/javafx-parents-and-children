package drahos.ui.components.errrorbox;

import drahos.dynasty.auth.AuthenticationError;
import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.error.DbError;
import drahos.dynasty.persons.actions.validator.PersonError;

import java.util.HashMap;
import java.util.Map;

class ErrorMessages {

    public static final Map<ApplicationError, String> messages = new HashMap<ApplicationError, String>() {
        {
            put(AuthenticationError.NONEXISTING_USERNAME, "Neexistující uživatelské jméno");
            put(AuthenticationError.UNATHORIZED_ACCESS, "Nesprávné heslo, zobrazte si heslo, zda nedošlo k překlepu");

            put(DbError.DATABASE_ERROR, "Něco se nepovedlo v databázi :( Zkuste to později...");

            put(PersonError.NO_FIRST_NAME, "Vyplňte jméno");
            put(PersonError.NO_LAST_NAME, "Vyplňte příjmení");
            put(PersonError.NO_BIRTH_DATE, "Vyplňte datum narození ve formátu dd.MM.yyyy (např. 1.1.2013)");
            put(PersonError.IDENTICAL_PARENTS, "Otec a matka nemůžou být stejné osoby");
            put(PersonError.DATE_ORDER, "Datum narození musí být menší než datum úmrtí");
            put(PersonError.NO_CHANGE, "Neprovedli jste žádnou změnu");
        }
    };

}
