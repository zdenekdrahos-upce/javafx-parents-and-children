package drahos.ui.components.errrorbox;

import drahos.dynasty.error.ApplicationError;
import javafx.fxml.FXML;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import java.util.HashMap;
import java.util.Map;

public class ErrorBox {

    @FXML
    private HBox errorBox;
    @FXML
    private Label errorMessage;

    private final Map<ApplicationError, Field> map = new HashMap<>();

    public void add(ApplicationError error, Label label, Control control) {
        map.put(error, new Field(label, control));
    }

    public void noErrors() {
        errorBox.setVisible(false);
        for (Field field : map.values()) {
            removeErrorClass(field.label);
            removeErrorClass(field.control);
        }
    }

    private void removeErrorClass(Control control) {
        control.getStyleClass().removeAll("error");
    }

    public void processErrors(ApplicationError[] errors) {
        errorBox.setVisible(true);
        String errorsString = "";
        for (ApplicationError error : errors) {
            errorsString += ErrorMessages.messages.get(error) + "\n";
            if (map.containsKey(error)) {
                Field field = map.get(error);
                addErrorClass(field.label);
                addErrorClass(field.control);
            }
        }
        errorMessage.setText(errorsString);
    }

    private void addErrorClass(Control control) {
        control.getStyleClass().add("error");
    }
}
