package drahos.ui.components.errrorbox;

import javafx.scene.control.Control;
import javafx.scene.control.Label;

class Field {
    public final Label label;
    public final Control control;

    public Field(Label label, Control control) {
        this.label = label;
        this.control = control;
    }
}
