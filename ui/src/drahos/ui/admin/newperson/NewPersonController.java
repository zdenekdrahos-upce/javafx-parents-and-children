package drahos.ui.admin.newperson;

import drahos.dynasty.persons.actions.create.CreatePerson;
import drahos.dynasty.persons.actions.create.CreatePersonRequest;
import drahos.dynasty.persons.actions.create.CreatePersonResponse;
import drahos.dynasty.persons.actions.validator.PersonError;
import drahos.dynasty.InteractorFactory;
import drahos.ui.DynastyApplication;
import drahos.ui.helpers.FilterableParents;
import drahos.ui.components.errrorbox.ErrorBox;
import drahos.ui.helpers.OnlineDateTextFieldValidator;
import drahos.ui.components.personstable.PersonRow;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewPersonController {

    @FXML
    private Label successLabel, firstNameLabel, lastNameLabel, dateBirthLabel, motherLabel, dateDeathLabel;
    @FXML
    private TextField firstName, lastName, dateBirth, dateDeath;
    @FXML
    private ComboBox father, mother;
    @FXML
    private ErrorBox errorsController;

    private ObservableList<PersonRow> personsTable;
    private FilterableParents filterableParents;
    private final CreatePerson interactor;
    private final CreatePersonRequest request;
    private final SimpleDateFormat dateFormat;

    public NewPersonController() {
        InteractorFactory interactorFactory = new InteractorFactory();
        interactor = interactorFactory.buildPersonCreate();
        request = new CreatePersonRequest();
        dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    }

    public void initialize() {
        errorsController.add(PersonError.NO_FIRST_NAME, firstNameLabel, firstName);
        errorsController.add(PersonError.NO_LAST_NAME, lastNameLabel, lastName);
        errorsController.add(PersonError.NO_BIRTH_DATE, dateBirthLabel, dateBirth);
        errorsController.add(PersonError.IDENTICAL_PARENTS, motherLabel, father);
        errorsController.add(PersonError.DATE_ORDER, dateDeathLabel, dateDeath);
        dateBirth.textProperty().addListener(new OnlineDateTextFieldValidator(dateBirth, dateFormat));
        dateDeath.textProperty().addListener(new OnlineDateTextFieldValidator(dateDeath, dateFormat));
    }

    public void setPersonsTable(ObservableList<PersonRow> personsTable) {
        this.personsTable = personsTable;
        filterableParents = new FilterableParents(personsTable);
        prepareParentCombobox(father);
        prepareParentCombobox(mother);
        dateBirth.focusedProperty().addListener(new ParentByDateFilter(filterableParents, dateBirth, dateFormat));
    }

    private void prepareParentCombobox(ComboBox comboBox) {
        comboBox.setItems(filterableParents.getAvailableParents());
    }

    public void onCreate(ActionEvent actionEvent) {
        request.firstName = firstName.getText();
        request.lastName = lastName.getText();
        request.birthDate = parseDate(dateBirth);
        request.deathDate = parseDate(dateDeath);
        request.idFather = getIdParent(father);
        request.idMother = getIdParent(mother);

        CreatePersonResponse response = interactor.execute(request);
        if (response.wasCreated) {
            PersonRow row = new PersonRow(response.person);
            personsTable.add(row);
            errorsController.noErrors();
            successLabel.setVisible(true);
            successLabel.setText("Vytvořili jste novou osobu!");
            DynastyApplication.logAction("Vytvoření osoby - " + row);
        } else {
            successLabel.setVisible(false);
            errorsController.processErrors(response.errors);
        }
    }

    private Date parseDate(TextField textField) {
        try {
            return dateFormat.parse(textField.getText());
        } catch (ParseException e) {
        }
        return null;
    }

    private Integer getIdParent(ComboBox comboBox) {
        SingleSelectionModel selectionModel = comboBox.getSelectionModel();
        if (selectionModel.isEmpty()) {
            return null;
        } else {
            PersonRow parent = (PersonRow) selectionModel.getSelectedItem();
            return parent.getIdPerson();
        }
    }

    public void clearForm(ActionEvent actionEvent) {
        successLabel.setVisible(false);
        errorsController.noErrors();
        firstName.setText("");
        lastName.setText("");
        dateBirth.setText("");
        dateDeath.setText("");
        father.getSelectionModel().clearSelection();
        mother.getSelectionModel().clearSelection();
        filterableParents.removeFilter();
    }
}
