package drahos.ui.admin.newperson;

import drahos.ui.helpers.FilterableParents;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

class ParentByDateFilter implements ChangeListener<Boolean> {

    private final TextField dateBirth;
    private final DateFormat dateFormat;
    private final FilterableParents parents;

    public ParentByDateFilter(FilterableParents parents, TextField dateBirth, DateFormat dateFormat) {
        this.parents = parents;
        this.dateBirth = dateBirth;
        this.dateFormat = dateFormat;
    }

    @Override
    public void changed(ObservableValue<? extends Boolean> observableValue, Boolean oldValue, Boolean newValue) {
        if (isFocusedOut(newValue)) {
            String inputDate = dateBirth.getText();
            if (inputDate.isEmpty()) {
                parents.removeFilter();
            } else {
                try {
                    Date date = dateFormat.parse(inputDate);
                    parents.filterByDate(date);
                } catch (ParseException e) {
                }
            }
        }
    }

    private boolean isFocusedOut(Boolean newValue) {
        return !newValue;
    }
}
