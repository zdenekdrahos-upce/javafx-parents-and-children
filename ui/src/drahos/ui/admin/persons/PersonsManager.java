package drahos.ui.admin.persons;

import drahos.ui.components.personstable.PersonRow;
import javafx.collections.ObservableList;

public interface PersonsManager {

    void setRows(ObservableList<PersonRow> rows);

    void delete(PersonRow person);

    void update(PersonRow person);
}
