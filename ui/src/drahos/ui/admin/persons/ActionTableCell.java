package drahos.ui.admin.persons;

import drahos.ui.components.personstable.PersonRow;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;

import java.io.IOException;

class ActionTableCell extends TableCell {

    private HBox buttons;
    private Button update, delete;
    private final PersonsManager manager;

    public ActionTableCell(PersonsManager manager) {
        this.manager = manager;
        try {
            buttons = FXMLLoader.load(getClass().getResource("actions-cell.fxml"));
            update = (Button) buttons.lookup("#update");
            delete = (Button) buttons.lookup("#delete");
            listenOnDelete();
            listenOnUpdate();
        } catch (IOException e) {
        }
    }

    private void listenOnDelete() {
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (isPersonInRow()) {
                    PersonRow person = (PersonRow) getTableRow().getItem();
                    manager.delete(person);
                }
            }
        });
    }

    private void listenOnUpdate() {
        update.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (isPersonInRow()) {
                    PersonRow person = (PersonRow) getTableRow().getItem();
                    manager.update(person);
                }
            }
        });
    }

    @Override
    protected void updateItem(Object o, boolean empty) {
        super.updateItem(o, empty);
        if (isPersonInRow()) {
            setGraphic(buttons);
        }
    }

    private boolean isPersonInRow() {
        return getTableRow() != null && getTableRow().getItem() != null;
    }
}
