package drahos.ui.admin.persons;

import drahos.dynasty.persons.actions.delete.DeletePerson;
import drahos.dynasty.persons.Persons;
import drahos.dynasty.persons.PersonsRequest;
import drahos.dynasty.persons.PersonsResponse;
import drahos.dynasty.persons.actions.delete.DeletePersonRequest;
import drahos.dynasty.persons.actions.delete.DeletePersonResponse;
import drahos.dynasty.persons.actions.update.UpdatePerson;
import drahos.dynasty.persons.actions.update.UpdatePersonRequest;
import drahos.dynasty.persons.actions.update.UpdatePersonResponse;
import drahos.dynasty.InteractorFactory;
import drahos.ui.DynastyApplication;
import drahos.ui.components.errrorbox.ErrorBox;
import drahos.ui.helpers.FilterableParents;
import drahos.ui.components.personstable.PersonRow;
import drahos.ui.components.personstable.TableController;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;

public class ManagePersonsController {

    @FXML
    private TableController tableController;
    @FXML
    private Label successLabel;
    @FXML
    private ErrorBox errorsController;

    private final Persons persons;
    private final PersonsRequest request;
    private final PersonsManager manager;

    public ManagePersonsController() {
        InteractorFactory interactorFactory = new InteractorFactory();
        persons = interactorFactory.buildPersons();
        manager = new Manager(interactorFactory);
        request = new PersonsRequest();
    }

    public void initialize() {
        PersonsResponse response = persons.execute(request);
        tableController.insertPersons(response.persons);
        filterableParents = new FilterableParents(tableController.getRows());
        manager.setRows(tableController.getRows());

        ObservableList columns = tableController.getColumns();
        tableController.enableEditation();
        makeColumnsEditable(columns);
        columns.add(getActionsColumns());
    }

    private void makeColumnsEditable(ObservableList columns) {
        for (Object column : columns) {
            editColumnAsTextField(column);
            ObservableList children = ((TableColumn) column).getColumns();
            if (!children.isEmpty()) {
                for (Object child : children) {
                    editParentAsCombobox(child);
                }
            }
        }
    }

    private void editColumnAsTextField(Object column) {
        TableColumn c = (TableColumn) column;
        c.setCellFactory(TextFieldTableCell.forTableColumn());
    }

    private FilterableParents filterableParents;

    private void editParentAsCombobox(Object column) {
        TableColumn c = (TableColumn) column;
        c.setCellFactory(ComboBoxTableCell.<PersonRow, PersonRow>forTableColumn(filterableParents.getAvailableParents()));
        c.setOnEditStart(new EventHandler<TableColumn.CellEditEvent>() {
            @Override
            public void handle(TableColumn.CellEditEvent cellEditEvent) {
                PersonRow person = (PersonRow) cellEditEvent.getRowValue();
                if (person != null) {
                    filterableParents.filterByPerson(person);
                }
            }
        });
        c.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent>() {
            @Override
            public void handle(TableColumn.CellEditEvent cellEditEvent) {
                PersonRow child = (PersonRow) cellEditEvent.getRowValue();
                PersonRow newParent = (PersonRow) cellEditEvent.getNewValue();
                if (child != null && newParent != null) {
                    int column = cellEditEvent.getTablePosition().getColumn();
                    if (column == 4) {
                        child.changeFather(newParent);
                    } else {
                        child.changeMother(newParent);
                    }
                }
            }
        });
    }

    private TableColumn getActionsColumns() {
        TableColumn actions = new TableColumn("Akce");
        actions.setSortable(false);
        actions.setMinWidth(230);
        actions.setEditable(false);
        actions.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn tableColumn) {
                return new ActionTableCell(manager);
            }
        });
        return actions;
    }

    public ObservableList<PersonRow> getTable() {
        return tableController.getRows();
    }

    private class Manager implements PersonsManager {

        private ObservableList<PersonRow> rows;
        private final DeletePerson deleteInteractor;
        private final DeletePersonRequest deleteRequest = new DeletePersonRequest();
        private final UpdatePerson updateInteractor;
        private final UpdatePersonRequest updateRequest = new UpdatePersonRequest();

        public Manager(InteractorFactory interactorFactory) {
            deleteInteractor = interactorFactory.buildPersonDelete();
            updateInteractor = interactorFactory.buildPersonUpdate();
        }

        @Override
        public void setRows(ObservableList<PersonRow> rows) {
            this.rows = rows;
        }

        @Override
        public void delete(PersonRow person) {
            deleteRequest.idPerson = person.getIdPerson();
            DeletePersonResponse response = deleteInteractor.execute(deleteRequest);
            if (response.wasDeleted) {
                DynastyApplication.logAction("Smazání osoby - " + person);
                rows.remove(person);
                deleteParentForChilds(person);
            }
        }

        private void deleteParentForChilds(PersonRow deletedPerson) {
            for (PersonRow existingPerson : rows) {
                if (existingPerson.hasParent(deletedPerson)) {
                    existingPerson.deleteParent(deletedPerson);
                }
            }
        }

        @Override
        public void update(PersonRow person) {
            updateRequest.updatedPerson = person.getUpdatePerson();
            UpdatePersonResponse response = updateInteractor.execute(updateRequest);

            if (response.wasUpdated) {
                DynastyApplication.logAction("Úprava osoby - " + person);
                errorsController.noErrors();
                int indexOfPerson = rows.indexOf(person);
                rows.set(indexOfPerson, new PersonRow(updateRequest.updatedPerson));
                successLabel.setVisible(true);
                updateParentForChilds(person);
            } else {
                successLabel.setVisible(false);
                errorsController.processErrors(response.errors);
            }

        }

        private void updateParentForChilds(PersonRow updatedPerson) {
            for (PersonRow existingPerson : rows) {
                if (existingPerson.hasParent(updatedPerson)) {
                    existingPerson.updateParent(updatedPerson);
                }
            }
        }
    }
}
