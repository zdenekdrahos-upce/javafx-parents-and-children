package drahos.ui.admin;

import drahos.ui.admin.log.LogController;
import drahos.ui.admin.newperson.NewPersonController;
import drahos.ui.admin.persons.ManagePersonsController;
import drahos.ui.components.personstable.PersonRow;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TabPane;

public class AdminController {

    @FXML
    private TabPane tabPane;
    @FXML
    private ManagePersonsController personsController;
    @FXML
    private NewPersonController newPersonController;
    @FXML
    private LogController logTabController;

    public void initialize() {
        ObservableList<PersonRow> table = personsController.getTable();
        newPersonController.setPersonsTable(table);
        alwaysRefreshLogTab();
    }

    private void alwaysRefreshLogTab() {
        tabPane.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldTab, Number newTab) {
                if (isLogTab(newTab)) {
                    logTabController.reloadData();
                }
            }

            private boolean isLogTab(Number newTab) {
                return newTab.intValue() == 2;
            }
        });
    }
}
