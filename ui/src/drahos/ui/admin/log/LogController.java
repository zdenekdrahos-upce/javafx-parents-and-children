package drahos.ui.admin.log;

import drahos.dynasty.users.UsersActions;
import drahos.dynasty.users.UsersActionsResponse;
import drahos.dynasty.log.ActionView;
import drahos.dynasty.users.UserActionsView;
import drahos.dynasty.InteractorFactory;
import drahos.ui.helpers.CenteredTableCell;
import drahos.ui.helpers.DateStringComparator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;
import javafx.util.Callback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class LogController {

    @FXML
    private ListView usersList;
    @FXML
    private TableView actionsTable;

    private UserActionsView[] userActions;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    public void initialize() {
        userActions = getUserActions();
        initUsersList();
        initActionsTable();
        listenSelectedItemIsUsersList();
        formatDateColumn();
    }

    private UserActionsView[] getUserActions() {
        InteractorFactory interactorFactory = new InteractorFactory();
        UsersActions interactor = interactorFactory.buildUsersActions();
        UsersActionsResponse response = interactor.execute();
        return response.users;
    }

    private void initUsersList() {
        List<String> users = new ArrayList<>();
        for (UserActionsView userAction : userActions) {
            users.add(userAction.name);
        }
        ObservableList<String> rows = FXCollections.observableArrayList(users);
        usersList.setItems(rows);
    }

    private void initActionsTable() {
        actionsTable.setPlaceholder(new Text("Uživatel aplikaci nepoužívá :("));
        setFactoryForActionDate();
        setFactoryForActionColumn();
        insertActions(userActions[0].actions);
    }

    private void setFactoryForActionDate() {
        TableColumn<ActionView, String> firstNameCol = (TableColumn<ActionView, String>) actionsTable.getColumns().get(0);
        firstNameCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ActionView, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ActionView, String> p) {
                String date = dateFormat.format(p.getValue().date);
                return new SimpleStringProperty(date);
            }
        });
    }

    private void setFactoryForActionColumn() {
        TableColumn<ActionView, String> secondColumn = (TableColumn<ActionView, String>) actionsTable.getColumns().get(1);
        secondColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ActionView, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ActionView, String> p) {
                String action = p.getValue().action;
                return new SimpleStringProperty(action);
            }
        });
    }

    private void insertActions(ActionView[] actions) {
        ObservableList<ActionView> rows = FXCollections.observableArrayList(actions);
        actionsTable.setItems(rows);
    }

    private void listenSelectedItemIsUsersList() {
        usersList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object o2) {
                String username = (String) observableValue.getValue();
                if (username != null) {
                    ActionView[] userActions = getUserActions(username);
                    insertActions(userActions);
                }
            }
        });
    }

    private ActionView[] getUserActions(String username) {
        for (UserActionsView userAction : userActions) {
            if (username.equals(userAction.name)) {
                return userAction.actions;
            }
        }
        return new ActionView[0];
    }

    private void formatDateColumn() {
        TableColumn dateColumn = (TableColumn) actionsTable.getColumns().get(0);
        dateColumn.setCellFactory(new CenteredTableCell());
        dateColumn.setComparator(new DateStringComparator(dateFormat.toPattern()));
    }

    public void reloadData() {
        userActions = getUserActions();
        ActionView[] actions;
        if (usersList.getSelectionModel().isEmpty()) {
            actions = userActions[0].actions;
        } else {
            String selectedUser = usersList.getSelectionModel().getSelectedItem().toString();
            actions = getUserActions(selectedUser);
        }
        insertActions(actions);
    }
}
