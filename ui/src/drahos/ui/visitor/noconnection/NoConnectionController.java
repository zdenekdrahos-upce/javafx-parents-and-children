package drahos.ui.visitor.noconnection;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class NoConnectionController {

    @FXML
    private Label connectionString, username, password;;

    public void setConnectionParameters(String connection, String user, String pass) {
        connectionString.setText(connection);
        username.setText(user);
        password.setText(pass);
    }

}
