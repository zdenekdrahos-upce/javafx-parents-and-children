package drahos.ui.visitor.login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Control;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class PasswordToggler extends Control {

    @FXML
    private ToggleButton toggleButton;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField textField;

    public void initialize() {
        textField.visibleProperty().bind(toggleButton.selectedProperty());
        textField.managedProperty().bind(toggleButton.selectedProperty());
        passwordField.visibleProperty().bind(toggleButton.selectedProperty().not());
        passwordField.managedProperty().bind(toggleButton.selectedProperty().not());
        passwordField.textProperty().bindBidirectional(textField.textProperty());
    }

    public void togglePassword(ActionEvent actionEvent) {
        if (toggleButton.isSelected()) {
            toggleButton.setText("Skrýt heslo");
        } else {
            toggleButton.setText("Zobrazit heslo");
        }
    }

    public String getPassword() {
        return passwordField.getText();
    }
}

