package drahos.ui.visitor.login;

import drahos.dynasty.auth.Authentication;
import drahos.dynasty.auth.AuthenticationError;
import drahos.dynasty.auth.AuthenticationRequest;
import drahos.dynasty.auth.AuthenticationResponse;
import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.InteractorFactory;
import drahos.ui.DynastyApplication;
import drahos.ui.components.errrorbox.ErrorBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class LoginController {

    @FXML
    private Label usernameLabel, passwordLabel;
    @FXML
    private TextField username;
    @FXML
    private PasswordToggler passwordController;
    @FXML
    private ErrorBox errorsController;

    private final Authentication authentication;
    private final AuthenticationRequest visitorRequest, adminRequest;

    public LoginController() {
        InteractorFactory interactorFactory = new InteractorFactory();
        authentication = interactorFactory.buildAuthentication();
        visitorRequest = getVisitorRequest();
        adminRequest = getAdminRequest();
    }

    public void initialize() {
        errorsController.add(AuthenticationError.NONEXISTING_USERNAME, usernameLabel, username);
        errorsController.add(AuthenticationError.UNATHORIZED_ACCESS, passwordLabel, passwordController);
    }

    private AuthenticationRequest getVisitorRequest() {
        AuthenticationRequest r = new AuthenticationRequest();
        r.username = "visitor";
        r.password = "visitor";
        r.authenticateAsAdmin = false;
        return r;
    }

    private AuthenticationRequest getAdminRequest() {
        AuthenticationRequest r = new AuthenticationRequest();
        r.authenticateAsAdmin = true;
        return r;
    }

    public void loginAsVisitor(ActionEvent actionEvent) {
        AuthenticationResponse response = authentication.execute(visitorRequest);
        if (response.isAuthenticated) {
            login(visitorRequest.username);
            DynastyApplication.replaceWindow("visitor/persons/persons.fxml");
        } else {
            displayError(response.error);
        }
    }

    public void loginAsAdmin(ActionEvent actionEvent) {
        loadAdminCredentials();
        AuthenticationResponse response = authentication.execute(adminRequest);
        removeErrorStyle();
        if (response.isAuthenticated) {
            login(adminRequest.username);
            DynastyApplication.replaceWindow("admin/admin.fxml");
        } else {
            displayError(response.error);
        }
    }

    private void loadAdminCredentials() {
        adminRequest.username = username.getText();
        adminRequest.password = passwordController.getPassword();
    }

    private void login(String username) {
        DynastyApplication.login(username);
    }

    private void removeErrorStyle() {
        errorsController.noErrors();
    }

    private void displayError(AuthenticationError error) {
        errorsController.processErrors(new ApplicationError[] {error});
    }
}
