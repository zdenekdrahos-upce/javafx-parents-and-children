package drahos.ui.visitor.persons;

import drahos.dynasty.persons.PersonView;
import drahos.dynasty.persons.Persons;
import drahos.dynasty.persons.PersonsRequest;
import drahos.dynasty.persons.PersonsResponse;
import drahos.dynasty.persons.actions.analyze.*;
import drahos.dynasty.InteractorFactory;
import drahos.ui.components.personstable.PersonRow;
import drahos.ui.components.personstable.TableController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

public class PersonsController {

    @FXML
    private PieChart agePieChart;
    @FXML
    private BarChart partnersBarChart;
    @FXML
    private TableController masterController, detailController;

    private final Persons persons;
    private final PersonsRequest personsRequest;

    private final RelationshipAnalyzer analyzer;
    private final RelationshipAnalyzerRequest analyzerRequest;
    private final ObservableList<XYChart.Data<Integer,String>> partnersBarChartData = FXCollections.observableArrayList();
    private final ObservableList<PieChart.Data> agePieChartData = FXCollections.observableArrayList();

    public PersonsController() {
        InteractorFactory interactorFactory = new InteractorFactory();
        persons = interactorFactory.buildPersons();
        personsRequest = new PersonsRequest();
        analyzer = interactorFactory.buildRelationshipAnalyzer();
        analyzerRequest = new RelationshipAnalyzerRequest();
    }

    public void initialize() {
        PersonsResponse response = persons.execute(personsRequest);
        masterController.insertPersons(response.persons);
        initPartnersBarChart();
        initAgePieChart();
        listenSelectedRowInMasterTable();
    }

    private void initPartnersBarChart() {
        XYChart.Series series = new XYChart.Series();
        series.setData(partnersBarChartData);
        partnersBarChart.getData().addAll(series);
    }

    private void initAgePieChart() {
        agePieChart.setData(agePieChartData);
    }

    private void listenSelectedRowInMasterTable() {
        masterController.listenOnSelectedRow(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object o2) {
                PersonRow p = (PersonRow) observableValue.getValue();
                if (p != null) {
                    analyzerRequest.idPerson = p.getIdPerson();
                    RelationshipAnalyzerResponse response = analyzer.execute(analyzerRequest);
                    updateChildrenTable(response.children);
                    updatePartnersBarChart(response.partners);
                    updateAgePieChart(response.childrenAge);
                }
            }
        });
    }

    private void updateChildrenTable(PersonView[] children) {
        detailController.clearRows();
        detailController.insertPersons(children);
    }

    private void updatePartnersBarChart(PartnerView[] partners) {
        partnersBarChartData.clear();
        for (PartnerView partner : partners) {
            String name = partner.partner.name.equals("") ? "Neznámý partner" : partner.partner.name;
            XYChart.Data<Integer,String> data = new XYChart.Data<>(partner.childCount, name);
            partnersBarChartData.add(data);
        }
    }

    private void updateAgePieChart(ChildrenAgeView[] childrenAge) {
        agePieChartData.clear();
        for (ChildrenAgeView ageView : childrenAge) {
            String age = ((Integer)ageView.age).toString();
            PieChart.Data data = new PieChart.Data(age, ageView.count);
            agePieChartData.add(data);
        }
    }
}
