package drahos.ui;

import drahos.dynasty.connection.JdbcConnection;
import drahos.ui.visitor.noconnection.NoConnectionController;

import java.util.Map;

class ConnectionManager {

    private String username = "inpda";
    private String password = "inpda";
    private String connectionString = "jdbc:oracle:thin:@localhost:1521:xe";
    private boolean isConnectionOpened;

    public void tryOpenConnection() {
        try {
            JdbcConnection.openConnection(connectionString, username, password);
            isConnectionOpened = true;
        } catch (RuntimeException e) {
            isConnectionOpened = false;
        }
    }

    public void tryCloseConnection() {
        if (isConnectionOpened) {
            JdbcConnection.closeConnection();
        }
    }

    public boolean isConnectionOpened() {
        return isConnectionOpened;
    }

    public void displayParameters(NoConnectionController connectionController) {
        connectionController.setConnectionParameters(connectionString, username, password);
    }

    public void updateConnectionParameters(Map<String, String> parameters) {
        username = parameters.containsKey("username") ? parameters.get("username") : username;
        password = parameters.containsKey("password") ? parameters.get("password") : password;
        connectionString = parameters.containsKey("connectionString") ?
                parameters.get("connectionString") : connectionString;
    }
}
