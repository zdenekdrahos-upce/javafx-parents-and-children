package drahos.ui;

import drahos.dynasty.log.LogAction;
import drahos.dynasty.log.LogActionRequest;
import drahos.dynasty.InteractorFactory;

class Logger {

    private final LogAction logAction;
    private final LogActionRequest logActionRequest;

    public Logger() {
        InteractorFactory interactorFactory = new InteractorFactory();
        logAction = interactorFactory.buildLoginAction();
        logActionRequest = new LogActionRequest();
    }

    public void logAction(String username, String action) {
        logActionRequest.username = username;
        logActionRequest.action = action;
        logAction.execute(logActionRequest);
    }
}
