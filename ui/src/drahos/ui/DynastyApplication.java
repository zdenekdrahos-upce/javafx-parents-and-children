package drahos.ui;

import drahos.ui.visitor.noconnection.NoConnectionController;
import drahos.ui.window.HeaderController;
import drahos.ui.window.WindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class DynastyApplication extends Application {

    private static WindowController windowController;
    private static HeaderController headerController;
    private static Logger logger;
    private static final ConnectionManager connection = new ConnectionManager();

    @Override
    public void start(Stage stage) throws Exception {
        deleteDefaultWindowStyle(stage);

        FXMLLoader fxmlLoader = buildFxmlLoader("window/window.fxml");
        Scene scene = new Scene((Parent) fxmlLoader.load());
        windowController = fxmlLoader.getController();
        windowController.setStage(stage);

        fxmlLoader = buildFxmlLoader("window/header.fxml");
        windowController.setHeader((Node) fxmlLoader.load());
        headerController = fxmlLoader.getController();

        stage.setScene(scene);
        stage.show();

        Map<String, String> namedParameters = getParameters().getNamed();
        connection.updateConnectionParameters(namedParameters);
        connection.tryOpenConnection();
        logger = new Logger();

        if (connection.isConnectionOpened()) {
            replaceWindow("visitor/login/login.fxml");
        } else {
            displayErrorMessage();
        }
    }

    private void deleteDefaultWindowStyle(Stage primaryStage) {
        primaryStage.initStyle(StageStyle.UNDECORATED);
    }

    private void displayErrorMessage() throws IOException {
        FXMLLoader fxmlLoader = buildFxmlLoader("visitor/noconnection/noconnection.fxml");
        setWindowContent((Node) fxmlLoader.load());
        NoConnectionController connectionController = fxmlLoader.getController();
        connection.displayParameters(connectionController);
    }

    @Override
    public void stop() throws Exception {
        connection.tryCloseConnection();
        super.stop();
    }

    public static void main(String[] args) {
        DynastyApplication.launch(args);
    }

    public static void replaceWindow(String fxml) {
        try {
            Node main = (Node) buildFxmlLoader(fxml).load();
            setWindowContent(main);
        } catch (IOException e) {
        }
    }

    private static void setWindowContent(Node main) {
        main.setId("main");
        windowController.setContent(main);
    }

    public static void login(String username) {
        headerController.login(username);
    }

    public static void logAction(String action) {
        logger.logAction(headerController.getUsername(), action);
    }

    private static FXMLLoader buildFxmlLoader(String file) {
        URL location = DynastyApplication.class.getResource(file);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        return fxmlLoader;
    }
}
