package drahos.ui.window;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class WindowController {

    @FXML
    private Button resizeButton;
    @FXML
    private BorderPane windowBox;
    @FXML
    private AnchorPane header;

    private Stage stage;
    private boolean isMaximized = false;
    private Rectangle2D backupWindowBounds = null;
    private double dragWindowX = 0, dragWindowY = 0;
    private double resizeOffsetX, resizeOffsetY;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setHeader(Node appHeader) {
        header.getChildren().addAll(appHeader);
        AnchorPane.setTopAnchor(appHeader, 0.0);
        AnchorPane.setLeftAnchor(appHeader, 0.0);
        AnchorPane.setRightAnchor(appHeader, 20.0);
    }

    public void setContent(Node content) {
        windowBox.setCenter(content);
    }

    public void initialize() {
        moveWindowByDraggingHeader();
        resizeWindowByDraggingResizeButton();
    }

    private void moveWindowByDraggingHeader() {
        header.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                dragWindowX = event.getSceneX();
                dragWindowY = event.getSceneY();
            }
        });
        header.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - dragWindowX);
                stage.setY(event.getScreenY() - dragWindowY);
            }
        });
    }

    private void resizeWindowByDraggingResizeButton() {
        resizeButton.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                resizeOffsetX = (stage.getX() + stage.getWidth()) - e.getScreenX();
                resizeOffsetY = (stage.getY() + stage.getHeight()) - e.getScreenY();
                e.consume();
            }
        });
        resizeButton.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                ObservableList<Screen> screens = Screen.getScreensForRectangle(stage.getX(), stage.getY(), 1, 1);
                final Screen screen;
                if (screens.size() > 0) {
                    screen = Screen.getScreensForRectangle(stage.getX(), stage.getY(), 1, 1).get(0);
                } else {
                    screen = Screen.getScreensForRectangle(0, 0, 1, 1).get(0);
                }
                Rectangle2D visualBounds = screen.getVisualBounds();
                double maxX = Math.min(visualBounds.getMaxX(), e.getScreenX() + resizeOffsetX);
                double maxY = Math.min(visualBounds.getMaxY(), e.getScreenY() - resizeOffsetY);
                stage.setWidth(Math.max(windowBox.getMinWidth(), maxX - stage.getX()));
                stage.setHeight(Math.max(windowBox.getMinHeight(), maxY - stage.getY()));
                e.consume();
            }
        });
    }

    public void onClose(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void onMaximize(ActionEvent actionEvent) {
        isMaximized = !isMaximized;
        if (isMaximized) {
            maximize();
        } else {
            resetToWindow();
        }
    }

    private void maximize() {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        backupWindowBounds = new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
    }

    private void resetToWindow() {
        stage.setX(backupWindowBounds.getMinX());
        stage.setY(backupWindowBounds.getMinY());
        stage.setWidth(backupWindowBounds.getWidth());
        stage.setHeight(backupWindowBounds.getHeight());
    }

    public void onMinimize(ActionEvent actionEvent) {
        stage.setIconified(true);
    }

    public void onFullScreen(ActionEvent actionEvent) {
        boolean isFullScreen = stage.isFullScreen();
        stage.setFullScreen(!isFullScreen);
    }
}
