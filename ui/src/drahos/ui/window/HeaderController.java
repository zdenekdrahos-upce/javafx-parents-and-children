package drahos.ui.window;

import drahos.ui.DynastyApplication;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class HeaderController {

    @FXML
    private Label userLabel;
    @FXML
    private Button logoutButton;

    public void login(String username) {
        userLabel.setText(username);
        logoutButton.setVisible(true);
        DynastyApplication.logAction("Login");
    }

    public void logout(ActionEvent actionEvent) {
        DynastyApplication.logAction("Logout");
        userLabel.setText("Nepřihlášen");
        logoutButton.setVisible(false);
        DynastyApplication.replaceWindow("visitor/login/login.fxml");
    }

    public String getUsername() {
        return userLabel.getText();
    }
}
