INSERT INTO user_groups(id_group, group_name)
VALUES ('A', 'admin');

INSERT INTO user_groups(id_group, group_name)
VALUES ('V', 'visitor');

INSERT INTO users(username, password, id_group)
VALUES ('admin', 'admin', 'A');

INSERT INTO users(username, password, id_group)
VALUES ('visitor', 'visitor', 'V');

INSERT INTO log(username, time, action)
VALUES ('admin', sysdate, 'Login');

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Petr', 'Kobliha', TO_DATE('1.10.1960', 'DD.MM.YYYY'), null, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Jana', 'Koblihová', TO_DATE('7.12.1968', 'DD.MM.YYYY'), null, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'David', 'Kobliha', TO_DATE('30.7.1992', 'DD.MM.YYYY'), 1, 2);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Pavel', 'Levoboček', TO_DATE('10.11.1980', 'DD.MM.YYYY'), 1, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, death_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Eva', 'Podlaha', TO_DATE('28.10.1918', 'DD.MM.YYYY'), TO_DATE('29.9.1938', 'DD.MM.YYYY'), null, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, death_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Přemek', 'Podlaha', TO_DATE('30.10.1918', 'DD.MM.YYYY'), TO_DATE('29.9.1938', 'DD.MM.YYYY'), null, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Petr', 'Kopecký', TO_DATE('01.10.1960', 'DD.MM.YYYY'), null, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Lucie', 'Kopecká', TO_DATE('21.1.1962', 'DD.MM.YYYY'), null, null);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Synátor', 'Kopecký', TO_DATE('08.07.1990', 'DD.MM.YYYY'), 7, 8);

INSERT INTO persons(id_person, first_name, last_name, birth_date, death_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Dcerka', 'Kopecký', TO_DATE('13.02.1992', 'DD.MM.YYYY'), TO_DATE('18.09.2000', 'DD.MM.YYYY'), 7, 8);

INSERT INTO persons(id_person, first_name, last_name, birth_date, id_father, id_mother)
VALUES (SEQ_PERSONS_id_person.nextval, 'Kopeckého', 'Levoboček', TO_DATE('02.10.2000', 'DD.MM.YYYY'), 7, null);