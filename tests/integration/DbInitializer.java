import oracle.jdbc.OracleDriver;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbInitializer {

    private static final String[] files = {"doc/db/ddl.sql", "doc/db/inserts.sql"};

    public static void main(String[] args) throws SQLException, IOException {
        String username = args[0];
        String password = args[1];

        DriverManager.registerDriver(new OracleDriver());
        Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", username, password);
        ScriptRunner runner = new ScriptRunner(connection, false, true);

        System.out.println("DB: " + username + ", password: " + password);
        for (String file : files) {
            System.out.println("Import: " + file);
            runner.runScript(new BufferedReader(new FileReader(file)));
            System.out.println();
        }

        connection.commit();
        connection.close();
    }

}
