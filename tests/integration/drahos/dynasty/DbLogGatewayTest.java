package drahos.dynasty;

import drahos.dynasty.log.entity.LogGateway;
import drahos.dynasty.log.entity.LoggedAction;
import drahos.dynasty.log.database.DbLogGateway;
import org.junit.Test;

import java.sql.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DbLogGatewayTest extends GatewayTest {

    private LogGateway gateway;

    @Override
    protected void createGateway() {
        gateway = new DbLogGateway(connection);
    }

    @Test
    public void numberOfActionsShouldBeIncrementedAfterInsert() {
        int previousCount = getNumberOfActionsInDb();
        gateway.insertAction("admin", "Login");
        int currentCount = getNumberOfActionsInDb();
        assertEquals(currentCount, previousCount + 1);
    }

    private int getNumberOfActionsInDb() {
        try {
            PreparedStatement s = connection.prepareStatement("select count(*) from log");
            ResultSet resultSet = s.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        }
        return -1;
    }

    @Test
    public void visitorShouldHaveNoActions() {
        LoggedAction[] actions = gateway.findUserActions("visitor");
        assertEquals(0, actions.length);
    }

    @Test
    public void firstAdminActionShouldBeLogin() {
        LoggedAction[] actions = gateway.findUserActions("admin");
        assertTrue(actions.length > 0);
        LoggedAction action = actions[0];
        assertEquals("admin", action.getUsername());
        assertEquals("Login", action.getAction());
    }
}
