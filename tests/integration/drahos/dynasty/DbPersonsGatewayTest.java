package drahos.dynasty;

import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;
import drahos.dynasty.persons.database.DbPersonsGateway;
import org.junit.Test;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class DbPersonsGatewayTest extends GatewayTest {

    private PersonsGateway gateway;

    @Override
    protected void createGateway() {
        gateway = new DbPersonsGateway(connection);
    }

    @Test
    public void databaseShouldContainAtLeastSixPersons() {
        Person[] persons = gateway.findAll();
        assertTrue(persons.length >= 6);
    }

    @Test
    public void janaKoblihovaShouldHaveOneChild() {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Person[] persons = gateway.findChildren(2);
        assertEquals(1, persons.length);
        Person david = persons[0];

        assertEquals(3, david.getId());
        assertEquals("David", david.getFirstName());
        assertEquals("30.07.1992", df.format(david.getBirthDate()));
        assertNull(david.getDeathDate());
        assertDavidHasParentsWithIdOneAndTwo(david);
    }

    @Test
    public void petrKoblihaShouldHaveTwoChildren() {
        Person[] persons = gateway.findChildren(1);
        assertEquals(2, persons.length);
    }

    @Test
    public void premekPodhlahaShouldNotHaveChildren() {
        Person[] persons = gateway.findChildren(6);
        assertEquals(0, persons.length);
    }

    @Test
    public void repeatingQueryShouldReturnSameResults() {
        int firstCount = gateway.findAll().length;
        int secondCount = gateway.findAll().length;
        assertEquals(firstCount, secondCount);
    }

    @Test
    public void deleteOfNonExistingPersonShouldReturnFalse() {
        boolean deleteResult = gateway.deleteById(-1);
        assertFalse(deleteResult);
    }

    @Test
    public void testCreatingAndDeletingPerson() {
        boolean result;
        Person p = new Person(0, "first", "last", new Date(), null, null, null);

        result = gateway.create(p);
        assertTrue(result);

        assertPersonHasID(p);
        assertTrue(isPersonInDB(p.getId()));

        result = gateway.deleteById(p.getId());
        assertTrue(result);

        assertFalse(isPersonInDB(p.getId()));
    }

    private void assertPersonHasID(Person p) {
        assertTrue(p.getId() != 0);
    }

    private boolean isPersonInDB(int idPerson) {
        try {
            PreparedStatement s = connection.prepareStatement("select count(*) from persons where id_person = ?");
            s.setInt(1, idPerson);
            ResultSet resultSet = s.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1) == 1;
            }
        } catch (SQLException e) {
        }
        return false;
    }

    @Test
    public void testPersonUpdate() {
        Person david = gateway.findById(3);
        assertDavidHasParentsWithIdOneAndTwo(david);

        updateDavidParents(david, null, null);
        david = gateway.findById(3);
        assertChildHasNotParents(david);

        updateDavidParents(david, 1, 2);
        david = gateway.findById(3);
        assertDavidHasParentsWithIdOneAndTwo(david);
    }

    private void assertDavidHasParentsWithIdOneAndTwo(Person david) {
        assertEquals(1, (int)david.getIdFather());
        assertEquals(2, (int) david.getIdMother());
    }

    private void assertChildHasNotParents(Person david) {
        assertNull(david.getIdFather());
        assertNull(david.getIdMother());
    }

    private void updateDavidParents(Person david, Integer idFather, Integer idMother) {
        boolean result;
        david.setIdFather(idFather);
        david.setIdMother(idMother);
        result = gateway.update(david);
        assertTrue(result);
    }

    @Test
    public void childShouldHaveNoParentsIfParentsAreDeleted() {
        Person father = new Person(0, "father", "father", new Date(), null, null, null);
        createPerson(father);
        Person mother = new Person(0, "mother", "mother", new Date(), null, null, null);
        createPerson(mother);
        Person child = new Person(0, "child", "child", new Date(), null, father.getId(), mother.getId());
        createPerson(child);

        deletePerson(father);
        deletePerson(mother);

        child = gateway.findById(child.getId());
        assertChildHasNotParents(child);
    }

    private void createPerson(Person person) {
        boolean result = gateway.create(person);
        assertTrue(result);
    }

    private void deletePerson(Person person) {
        boolean result = gateway.deleteById(person.getId());
        assertTrue(result);
    }
}
