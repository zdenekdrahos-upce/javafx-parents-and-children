package drahos.dynasty;

import drahos.dynasty.connection.JdbcConnection;
import drahos.dynasty.connection.OpenedConnection;
import org.junit.Before;

import java.sql.SQLException;

abstract public class GatewayTest {

    protected OpenedConnection connection;

    @Before
    public void setUp() throws SQLException {
        JdbcConnection.openConnection("jdbc:oracle:thin:@localhost:1521:xe", "inpda_test", "inpda_test");
        connection = JdbcConnection.getConnection();
        createGateway();
    }

    abstract protected void createGateway();
}
