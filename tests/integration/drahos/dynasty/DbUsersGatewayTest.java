package drahos.dynasty;

import drahos.dynasty.users.database.DbUsersGateway;
import drahos.dynasty.users.entity.User;
import drahos.dynasty.users.entity.UsersGateway;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class DbUsersGatewayTest extends GatewayTest {

    private UsersGateway gateway;

    @Override
    protected void createGateway() {
        gateway = new DbUsersGateway(connection);
    }

    @Test
    public void nullShouldBeReturnedForUnexistingUsername() {
        assertNull(gateway.findByName(""));
    }

    @Test
    public void adminShouldExistInDb() {
        assertExistingUser("admin", "admin", true);
    }

    @Test
    public void visitorShouldExistInDb() {
        assertExistingUser("visitor", "visitor", false);
    }

    private void assertExistingUser(String name, String password, boolean isAdmin) {
        User user = gateway.findByName(name);
        assertEquals(name, user.getName());
        if (isAdmin) {
            assertTrue(user.isAdmin(password));
        } else {
            assertTrue(user.isVisitor(password));
        }
    }

    @Test
    public void adminAndVisitorShouldExistInDatabase() {
        User[] users = gateway.findAll();
        assertEquals(2, users.length);
        Assert.assertEquals("admin", users[0].getName());
        Assert.assertEquals("visitor", users[1].getName());
    }
}
