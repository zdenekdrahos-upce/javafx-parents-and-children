package drahos.dynasty;

import drahos.dynasty.log.entity.LogGateway;
import drahos.dynasty.log.entity.LoggedAction;
import drahos.dynasty.log.LogAction;
import drahos.dynasty.log.LogActionRequest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CreateActionTest {

    private final MockGateway gateway = new MockGateway();
    private final LogAction interactor = new LogAction(gateway);

    @Test
    public void actionShouldBeInsertedViaGateway() {
        LogActionRequest request = new LogActionRequest();
        request.username = "admin";
        request.action = "new action";
        interactor.execute(request);

        assertEquals(request.username, gateway.lastUsername);
        assertEquals(request.action, gateway.lastAction);
    }

    private static class MockGateway implements LogGateway {

        public String lastUsername, lastAction;

        @Override
        public void insertAction(String username, String action) {
            lastUsername = username;
            lastAction = action;
        }

        @Override
        public LoggedAction[] findUserActions(String username) {
            return new LoggedAction[0];
        }
    }
}
