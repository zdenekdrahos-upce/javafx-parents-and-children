package drahos.dynasty;

import drahos.dynasty.auth.Authentication;
import drahos.dynasty.auth.AuthenticationError;
import drahos.dynasty.auth.AuthenticationRequest;
import drahos.dynasty.auth.AuthenticationResponse;
import org.junit.Test;
import org.junit.Before;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AuthenticationTest extends Interactors {

    private final Authentication interactor = factory.buildAuthentication();
    private AuthenticationRequest request;

    @Before
    public void setUp() {
        request = new AuthenticationRequest();
    }

    @Test
    public void testValidAdminAuthentication() {
        request.username = "admin";
        request.password = "admin";
        request.authenticateAsAdmin = true;

        AuthenticationResponse response = interactor.execute(request);
        assertTrue(response.isAuthenticated);
    }

    @Test
    public void testValidVisitorAuthentication() {
        request.username = "visitor";
        request.password = "visitor";
        request.authenticateAsAdmin = false;

        AuthenticationResponse response = interactor.execute(request);
        assertTrue(response.isAuthenticated);
    }

    @Test
    public void visitorCannotBeAuthenticatedAsAdmin() {
        request.username = "visitor";
        request.password = "visitor";
        request.authenticateAsAdmin = true;

        assertAuthenticationFailure(AuthenticationError.UNATHORIZED_ACCESS);
    }

    @Test
    public void testNonExistingUsername() {
        request.username = "non-existing username";
        request.password = "visitor";
        request.authenticateAsAdmin = true;

        assertAuthenticationFailure(AuthenticationError.NONEXISTING_USERNAME);
    }

    private void assertAuthenticationFailure(AuthenticationError expectedError) {
        AuthenticationResponse response = interactor.execute(request);
        assertFalse(response.isAuthenticated);
        assertEquals(expectedError, response.error);
    }
}
