package drahos.dynasty;

import drahos.dynasty.persons.actions.create.CreatePerson;
import drahos.dynasty.persons.actions.create.CreatePersonRequest;
import drahos.dynasty.persons.actions.create.CreatePersonResponse;
import drahos.dynasty.persons.PersonView;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class CreatePersonTest extends Interactors {

    private final CreatePerson interactor = factory.buildPersonCreate();
    private CreatePersonRequest request;

    @Before
    public void setUp() {
        request = new CreatePersonRequest();
    }

    @Test
    public void createdPersonShouldHaveIdAndAttributesFromRequest() {
        request.firstName = "firstName";
        request.lastName = "lastName";
        request.birthDate = new Date();
        request.idFather = null;
        request.idMother = null;

        CreatePersonResponse response = interactor.execute(request);
        assertTrue(response.wasCreated);

        PersonView createdPerson = response.person;
        assertTrue(createdPerson.id > 0);
        assertEquals(request.firstName, createdPerson.firstName);
        assertEquals(request.lastName, createdPerson.lastName);
        assertEqualDates(request.birthDate, createdPerson.birthDate);
        assertNull(createdPerson.deathDate);
        assertEquals("", createdPerson.father.name);
        assertEquals("", createdPerson.mother.name);
    }

    private void assertEqualDates(Date a, Date b) {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        assertEquals(df.format(a), df.format(b));
    }
}
