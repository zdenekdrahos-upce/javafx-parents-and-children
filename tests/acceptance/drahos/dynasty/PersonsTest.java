package drahos.dynasty;

import drahos.dynasty.persons.Persons;
import drahos.dynasty.persons.PersonsRequest;
import drahos.dynasty.persons.PersonsResponse;
import drahos.dynasty.persons.PersonView;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PersonsTest extends Interactors {

    private final Persons interactor = factory.buildPersons();
    private PersonsRequest request;

    @Before
    public void setUp() {
        request = new PersonsRequest();
    }

    @Test
    public void davidKoblihaShouldBeSonOfJanaKoblihovaAndPetrKobliha() {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        request.idPerson = 3;
        PersonsResponse response = interactor.execute(request);

        assertEquals(1, response.persons.length);
        PersonView david = response.persons[0];

        assertEquals("David", david.firstName);
        assertEquals("Kobliha", david.lastName);
        assertEquals("30.07.1992", df.format(david.birthDate));
        assertNull(david.deathDate);
        assertEquals("Petr Kobliha", david.father.name);
        assertEquals("Jana Koblihová", david.mother.name);
    }
}
