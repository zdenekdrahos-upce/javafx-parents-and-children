package drahos.dynasty;

import drahos.dynasty.persons.actions.analyze.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RelationshipAnalyzerTest extends Interactors {

    private final RelationshipAnalyzer interactor = factory.buildRelationshipAnalyzer();
    private RelationshipAnalyzerResponse response;

    @Test
    public void analyzePetrKopeckyRelationships() {
        RelationshipAnalyzerRequest request = new RelationshipAnalyzerRequest();
        request.idPerson = 7;
        response = interactor.execute(request);

        assertThreeChildren();
        assertTwoChildWithLucieKopeckaAndOneWithUnknownPartner();
        assertChildrenHasDifferentAge();
    }

    private void assertThreeChildren() {
        assertEquals(3, response.children.length);
    }

    private void assertTwoChildWithLucieKopeckaAndOneWithUnknownPartner() {
        assertEquals(2, response.partners.length);
        assertPartner("Lucie Kopecká", 2);
        assertPartner("", 1);
    }

    private void assertPartner(String name, int childCount) {
        boolean partnerExists = false;
        for (PartnerView partner : response.partners) {
            if (partner.partner.name.equals(name)) {
                assertEquals(childCount, partner.childCount);
                partnerExists = true;
                break;
            }
        }
        assertTrue(partnerExists);
    }

    private void assertChildrenHasDifferentAge() {
        assertEquals(3, response.childrenAge.length);

        for (ChildrenAgeView ageView : response.childrenAge) {
            for (ChildrenAgeView otherAge : response.childrenAge) {
                if (ageView != otherAge) {
                    assertTrue(ageView.age != otherAge.age);
                }
            }
        }
    }

}
