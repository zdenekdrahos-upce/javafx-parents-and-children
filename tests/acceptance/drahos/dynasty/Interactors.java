package drahos.dynasty;

import drahos.dynasty.connection.JdbcConnection;

class Interactors {

    protected final InteractorFactory factory;

    public Interactors() {
        JdbcConnection.openConnection("jdbc:oracle:thin:@localhost:1521:xe", "inpda_test", "inpda_test");
        factory = new InteractorFactory();
    }
}
