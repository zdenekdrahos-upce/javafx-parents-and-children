package drahos.dynasty;

import drahos.dynasty.persons.ParentView;
import drahos.dynasty.persons.PersonView;
import drahos.dynasty.persons.actions.update.UpdatePerson;
import drahos.dynasty.persons.actions.update.UpdatePersonRequest;
import drahos.dynasty.persons.actions.update.UpdatePersonResponse;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UpdatePersonTest extends Interactors {

    private final UpdatePerson interactor = factory.buildPersonUpdate();

    @Test
    public void premekPodlahaShouldBeUpdatedIfAtLeastOneDateIsDifferentThenHisCurrentDates() {
        UpdatePersonRequest request = new UpdatePersonRequest();
        request.updatedPerson = new PersonView(6);
        request.updatedPerson.firstName = "Přemek";
        request.updatedPerson.lastName = "Podlaha";
        request.updatedPerson.birthDate = new Date(1000);
        request.updatedPerson.deathDate = new Date();
        request.updatedPerson.father = ParentView.noParent;
        request.updatedPerson.mother = ParentView.noParent;

        UpdatePersonResponse response = interactor.execute(request);
        assertTrue(response.wasUpdated);
        assertEquals(0, response.errors.length);
    }
}
