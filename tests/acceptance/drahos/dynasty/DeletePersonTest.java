package drahos.dynasty;

import drahos.dynasty.persons.actions.create.CreatePerson;
import drahos.dynasty.persons.actions.create.CreatePersonRequest;
import drahos.dynasty.persons.actions.create.CreatePersonResponse;
import drahos.dynasty.persons.actions.delete.DeletePerson;
import drahos.dynasty.persons.actions.delete.DeletePersonRequest;
import drahos.dynasty.persons.actions.delete.DeletePersonResponse;
import org.junit.Test;
import java.util.Date;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DeletePersonTest extends Interactors {

    private final DeletePerson interactor = factory.buildPersonDelete();
    private final DeletePersonRequest request = new DeletePersonRequest();

    @Test
    public void personCanBeDeletedOnlyOneTime() {
        request.idPerson = createPerson();

        assertExistingPersonIsDeleted();
        assertNonExistingPersonIsNotDeleted();
    }

    private void assertExistingPersonIsDeleted() {
        DeletePersonResponse response;
        response = interactor.execute(request);
        assertTrue(response.wasDeleted);
    }

    private void assertNonExistingPersonIsNotDeleted() {
        DeletePersonResponse response;
        response = interactor.execute(request);
        assertFalse(response.wasDeleted);
    }

    private int createPerson() {
        CreatePerson interactor = factory.buildPersonCreate();
        CreatePersonRequest request = new CreatePersonRequest();
        request.firstName = "firstName";
        request.lastName = "lastName";
        request.birthDate = new Date();
        CreatePersonResponse response = interactor.execute(request);
        if (response.wasCreated) {
            return response.person.id;
        } else {
            throw new RuntimeException("Error in creating person");
        }
    }

}
