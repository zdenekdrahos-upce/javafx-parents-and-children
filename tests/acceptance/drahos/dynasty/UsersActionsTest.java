package drahos.dynasty;

import drahos.dynasty.users.UsersActions;
import drahos.dynasty.users.UsersActionsResponse;
import drahos.dynasty.log.ActionView;
import drahos.dynasty.users.UserActionsView;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UsersActionsTest extends Interactors {

    private final UsersActions interactor = factory.buildUsersActions();
    private UsersActionsResponse response;

    @Before
    public void setUp() {
        response = interactor.execute();
    }

    @Test
    public void adminAndVisitorShouldExistInDatabase() {
        assertEquals(2, response.users.length);
        assertEquals("admin", response.users[0].name);
        assertEquals("visitor", response.users[1].name);
    }

    @Test
    public void visitorShouldHaveNoActions() {
        UserActionsView visitor = response.users[1];
        assertEquals(0, visitor.actions.length);
    }

    @Test
    public void firstAdminActionShouldBeLogin() {
        UserActionsView admin = response.users[0];
        assertTrue(admin.actions.length > 0);
        ActionView action = admin.actions[0];
        assertEquals("Login", action.action);
        assertActionIsFromCurrentYear(action.date);
    }

    private void assertActionIsFromCurrentYear(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy");
        Integer currentYear = Calendar.getInstance().get(Calendar.YEAR);
        assertEquals(currentYear.toString(), df.format(date));
    }
}
