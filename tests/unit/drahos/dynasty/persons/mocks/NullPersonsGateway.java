package drahos.dynasty.persons.mocks;

import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.entity.PersonsGateway;

import java.util.Date;

public class NullPersonsGateway implements PersonsGateway {

    private static final Date currentDate = new Date();

    @Override
    public Person[] findAll() {
        return new Person[0];
    }

    @Override
    public Person[] findChildren(int idParent) {
        return new Person[0];
    }

    @Override
    public Person findById(int idPerson) {
        return new Person(idPerson, "first", "last", currentDate, null, null, null);
    }

    @Override
    public boolean create(Person person) {
        return false;
    }

    @Override
    public boolean update(Person person) {
        return false;
    }

    @Override
    public boolean deleteById(int idPerson) {
        return false;
    }
}
