package drahos.dynasty.persons.actions.create;

import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.error.DbError;
import drahos.dynasty.persons.actions.validator.PersonError;
import drahos.dynasty.persons.mocks.NullPersonsGateway;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CreatePersonTest {

    private final CreatePerson interactor = new CreatePerson(new NullPersonsGateway());
    private final CreatePersonRequest request = new CreatePersonRequest();

    @Test
    public void emptyPersonWithIdenticalParentsShouldCauseFourErrors() {
        createInvalidRequestWithAllValidationErrors();

        ApplicationError[] expectedErrors = {
                PersonError.NO_FIRST_NAME, PersonError.NO_LAST_NAME,
                PersonError.NO_BIRTH_DATE, PersonError.IDENTICAL_PARENTS
        };
        assertExpectedErrors(expectedErrors);
    }

    private void createInvalidRequestWithAllValidationErrors() {
        request.firstName = "";
        request.lastName = "";
        request.birthDate = null;
        request.idFather = 1;
        request.idMother = 1;
    }

    @Test
    public void notCreatedPersonInDbShouldCauseOneError() {
        createValidRequest();

        ApplicationError[] expectedErrors = {DbError.DATABASE_ERROR};
        assertExpectedErrors(expectedErrors);
    }

    private void createValidRequest() {
        request.firstName = "firstName";
        request.lastName = "lastName";
        request.birthDate = new Date();
        request.idFather = 1;
        request.idMother = 2;
    }

    private void assertExpectedErrors(ApplicationError[] expectedErrors) {
        CreatePersonResponse response = interactor.execute(request);
        assertFalse(response.wasCreated);
        assertEquals(expectedErrors.length, response.errors.length);

        List<ApplicationError> errors = new ArrayList<>(Arrays.asList(response.errors));
        for (ApplicationError expectedError : expectedErrors) {
            assertTrue(errors.contains(expectedError));
        }
    }
}
