package drahos.dynasty.persons.actions.update;

import drahos.dynasty.error.ApplicationError;
import drahos.dynasty.error.DbError;
import drahos.dynasty.persons.ParentView;
import drahos.dynasty.persons.PersonView;
import drahos.dynasty.persons.actions.validator.PersonError;
import drahos.dynasty.persons.entity.Person;
import drahos.dynasty.persons.mocks.NullPersonsGateway;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class UpdatePersonTest {

    private final NullPersonsGateway nullGateway = new NullPersonsGateway();
    private final UpdatePerson interactor = new UpdatePerson(nullGateway);
    private final UpdatePersonRequest request = new UpdatePersonRequest();

    @Before
    public void setUp() {
        createRequestWithNoChange();
    }

    @Test
    public void noChangeInPersonAttributes() {
        assertOneError(PersonError.NO_CHANGE);
    }

    @Test
    public void newPersonMustBeValidatedAsNewlyCreatedPerson() {
        request.updatedPerson.firstName = "";
        assertOneError(PersonError.NO_FIRST_NAME);
    }

    @Test
    public void notCreatedPersonInDbShouldCauseOneError() {
        request.updatedPerson.firstName = "updated first name";

        assertOneError(DbError.DATABASE_ERROR);
    }

    private void createRequestWithNoChange() {
        Person p = nullGateway.findById(0);
        PersonView view = new PersonView(p.getId());

        view.firstName = p.getFirstName();
        view.lastName = p.getLastName();
        view.birthDate = p.getBirthDate();
        view.deathDate = p.getDeathDate();
        view.father = ParentView.noParent;
        view.mother = ParentView.noParent;
        request.updatedPerson = view;
    }

    private void assertOneError(ApplicationError expectedError) {
        UpdatePersonResponse response = interactor.execute(request);
        assertFalse(response.wasUpdated);
        assertEquals(1, response.errors.length);
        assertEquals(expectedError, response.errors[0]);
    }
}
