package drahos.dynasty.persons.actions.validator;

import drahos.dynasty.persons.entity.Person;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonValidatorTest {

    private PersonValidationResult result;
    private final PersonValidator validator = new PersonValidator();
    private final Person emptyPerson = new Person(0, "", "", null, null, null, null);

    @Test
    public void testSuccessfullValidation() {
        Person p = new Person(0, "first", "last", new Date(10), new Date(20), 2, 5);
        result = validator.validate(p);
        assertTrue(result.isValid());
        assertEquals(0, result.errors.length);
    }

    @Test
    public void ifPersonIsNotValidThenResultMustContainErrors() {
        result = validator.validate(emptyPerson);
        assertFalse(result.isValid());
        assertTrue(result.errors.length > 0);
    }

    @Test
    public void ifFirstNameIsEmptyThenFirstNameErrorMustExist() {
        result = validator.validate(emptyPerson);
        assertExistingError(PersonError.NO_FIRST_NAME);
    }

    @Test
    public void ifLastNameIsEmptyThenLastNameErrorMustExist() {
        result = validator.validate(emptyPerson);
        assertExistingError(PersonError.NO_LAST_NAME);
    }

    @Test
    public void ifBirthDateIsNullThenBirthDateErrorMustExist() {
        result = validator.validate(emptyPerson);
        assertExistingError(PersonError.NO_BIRTH_DATE);
    }

    private void assertExistingError(PersonError expectedError) {
        boolean existsError = false;
        for (PersonError error : result.errors) {
            if (error == expectedError) {
                existsError = true;
                break;
            }
        }
        assertTrue(existsError);
    }

    @Test
    public void parentsMustBeDifferentPersons() {
        Person p = new Person(0, "first", "last", new Date(), new Date(), 10, 10);
        result = validator.validate(p);

        assertOneError(PersonError.IDENTICAL_PARENTS);
    }

    @Test
    public void birthDateMustBeSmallerThenDeathDate() {
        Person p = new Person(0, "first", "last", new Date(20), new Date(10), 1, 2);
        result = validator.validate(p);
        assertOneError(PersonError.DATE_ORDER);
    }

    private void assertOneError(PersonError expectedError) {
        assertEquals(1, result.errors.length);
        assertEquals(expectedError, result.errors[0]);
    }
}
