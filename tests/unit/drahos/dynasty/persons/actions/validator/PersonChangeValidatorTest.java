package drahos.dynasty.persons.actions.validator;

import drahos.dynasty.persons.ParentView;
import drahos.dynasty.persons.PersonView;
import drahos.dynasty.persons.entity.Person;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonChangeValidatorTest {

    private PersonValidationResult result;
    private final PersonChangeValidator validator = new PersonChangeValidator();
    private final Person person = new Person(0, "first", "last", new Date(10), new Date(20), null, null);

    @Test
    public void changedAtLeastOneAttribute() {
        Person view = createPersonWithDifferentName();

        result = validator.validate(person, view);
        assertTrue(result.isValid());
    }

    private Person createPersonWithDifferentName() {
        return new Person(
                person.getId(),
                "updated name",
                person.getLastName(),
                person.getBirthDate(),
                person.getDeathDate(),
                person.getIdFather(),
                person.getIdMother()
        );
    }

    @Test
    public void noChange() {
        Person view = createPersonDuplicate();

        result = validator.validate(person, view);
        assertFalse(result.isValid());
        assertEquals(1, result.errors.length);
        assertEquals(PersonError.NO_CHANGE, result.errors[0]);
    }

    private Person createPersonDuplicate() {
        return new Person(
                person.getId(),
                person.getFirstName(),
                person.getLastName(),
                person.getBirthDate(),
                person.getDeathDate(),
                person.getIdFather(),
                person.getIdMother()
        );
    }

}
