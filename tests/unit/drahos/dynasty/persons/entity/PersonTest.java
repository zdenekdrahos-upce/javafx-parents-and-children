package drahos.dynasty.persons.entity;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class PersonTest {

    private final DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

    @Test
    public void testDeadPersonAge() throws ParseException {
        Person p = new Person(0, "", "", df.parse("1.1.2000"), df.parse("20.10.2010"), null, null);

        assertEquals(10, p.getAge());
    }

    @Test
    public void testAlivePersonAge() throws ParseException {
        int expectedAge = Calendar.getInstance().get(Calendar.YEAR) - 2000;
        Person p = new Person(0, "", "", df.parse("1.1.2000"), null, null, null);

        assertEquals(expectedAge, p.getAge());
    }

}
