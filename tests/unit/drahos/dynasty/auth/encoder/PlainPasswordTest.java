package drahos.dynasty.auth.encoder;

import drahos.dynasty.auth.entity.PasswordEncoder;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PlainPasswordTest {

    @Test
    public void passwordShouldNotBeEncoded() {
        PasswordEncoder encoder = new PlainPassword();
        String password = "password";
        assertEquals(password, encoder.encode(password));
    }

}
