package drahos.dynasty.users.entity;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserTest {

    private final User admin = new User("user", "password", true);
    private final User visitor = new User("user", "password", false);

    @Test
    public void testValidPassword() {
        assertTrue(visitor.isVisitor("password"));
    }

    @Test
    public void testNonMatchingPassword() {
        assertFalse(visitor.isVisitor("bad password"));
    }

    @Test
    public void adminCannotBeAuthenticatedAsOrdinaryUserEvenIfPasswordIsOK() {
        assertFalse(admin.isVisitor("password"));
    }

    @Test
    public void adminShouldBeAuthenticatedOnlyIfPasswordMatchesTheOriginal() {
        assertTrue(admin.isAdmin("password"));
    }

    @Test
    public void adminShouldNotBeAuthenticatedOnlyIfPasswordMatchesTheOriginal() {
        assertFalse(admin.isAdmin("bad password"));
    }

    @Test
    public void visitorCannotBeAuthenticatedAsAdminEvenIfPasswordIsOK() {
        assertFalse(visitor.isAdmin("password"));
    }
}
