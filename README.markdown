# JavaFX 2.2 - Parents and Children

## About

Project developed as semestral project in [Programming Database Applications](http://ects.upce.cz/predmet/KIT/INPDA?lang=en&rocnik=2) at [University of Pardubice](http://www.upce.cz/en/index.html).

![Master detail example](https://bitbucket.org/zdenekdrahos/javafx-parents-and-children/raw/tip/doc/screenshots/persons-master-detail.png "Master detail example")

## Database connection

### Predefined connection parameters

* Username: *inpda*
* Password: *inpda*
* Connection string: *jdbc:oracle:thin:@localhost:1521:xe*

### Named parameters for connection

    java -jar inpdadrahos.jar --username=inpda --password=inpda --connectionString=jdbc:oracle:thin:@localhost:1521:xe

## Facts

License: [WTFPL](http://www.wtfpl.net/).

Author: [Zdenek Drahos](https://bitbucket.org/zdenekdrahos).